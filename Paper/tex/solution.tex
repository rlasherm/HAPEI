\section{\sname{}}
\label{sec:solution}

\subsection{Phases}

In order to harden the application, a preparatory step is required to encrypt the instructions. Only then, the application can be executed.

\paragraph{Packing}

Packing is required to create the encrypted program, enriched with the necessary metadata, following the scheme detailed below.
It must be done on the final device, since it requires a device-specific secret key $k$.

\paragraph{Execution}

During execution, the encrypted instructions are deciphered on-the-fly and executed. The decryption can only occur if the program state is correct.

\subsection{\acrlong{PEI}}

SOFIA encodes the state of the program as the succession of $PC_{prev}$ and $PC$. We propose instead to encode the state of the program as the history of all previous executed instructions. 
Our proposal does not depend on the $PC$ value (apart when encoded in an instruction value). As such, the machine code is ensured to be executed correctly: instructions integrity is ensured together with control flow integrity.

Secondly, it becomes easy to check at anytime during execution that the current state of the program is valid.

We suppose that the \acrfull{CFG} of our program is perfectly defined at compile time. There is no ambiguity on the destination address of jumps and calls.
This condition is trivially satisfied if there are no indirect jumps or calls in our program. In the other case, it can be more tricky.

Let $acc_{n}$ (standing for accumulator at instruction $n$) be a value representing the state of the program when instruction $i_n$ is about to be executed ($n$ uniquely identify one instruction). 
$i_n$ and $acc_{n}$ can be seen as values in $\mathbb{F}_{2^w}$ and $\mathbb{F}_{2^b}$ respectively for some $w$ and $b$. $b$ is the instruction size (considered fixed) and $w$ is the security parameter.

\paragraph{Bootstrap}

To bootstrap the encoding, one has to use an initialization vector $IV$ as input for the first executed instruction.
\begin{equation}
    acc_{init} = HMAC_k(IV)
\end{equation}
$acc_{init}$ is considered as a predecessor program state to the entry instruction. It may be used in a multi-predecessor scheme or in the $1$-predecessor one.

\paragraph{$1$-predecessor}

The easy case is the $1$-predecessor case. Our program snippet is a succession of instructions $[i_1, i_2, \cdots, i_n, \cdots]$ where all instructions are executed in order.

The instructions are encoded as
\begin{equation}
i_n' = C\left( acc_{n}\right) \oplus i_n,
\end{equation}
where $C$ is a compression function: a projection from $\mathbb{F}_{2^b}$ to $\mathbb{F}_{2^w}$. $C$ must ensure that $x$ cannot be deduced from $C(x)$.

Obviously the state of the program must be updated, using secret key $k$:
\begin{equation}
acc_{n+1} = HMAC_k(acc_{n} || i_{n}).
\end{equation}

You can see that the state of the program is encoded with a hash chain depending on all previous instructions.
The encoded instruction $i_n'$ can only be decoded when the previous state of the program $acc_{n}$ is correct. This is possible only when instruction $i_n$ is due.
Decoding necessitates the same operations:
\begin{eqnarray}
acc_n &=& HMAC_k(acc_{n-1} || i_{n-1}), \\
i_n &=& C\left( acc_{n}\right) \oplus i_n'.
\end{eqnarray}

\paragraph{$2$-predecessors, a naive and limited solution}

Most programs necessitate branching. As a consequence, some instructions have $2$ predecessors (2 possible previous instructions at two different locations in the program).

As a consequence the previous state of the program may have 2 different values: $acc_{n}^1$ or $acc_{n}^2$.
1 out of the 2 possible values is required to decode $i_n$.  

Let $\Sigma = acc_{n}^1 \oplus acc_{n}^2$, we can encode our instruction as:
\begin{equation}
\{\Sigma,i_n' = C\left( acc_{n}^1\cdot acc_{n}^2 \right) \oplus i_n\},
\end{equation}
i.e. the previous state is encoded as $acc_{n}^1 \cdot acc_{n}^2$.

Two cases are possible: the previous state is $acc_{n} = acc_{n}^1$ or $acc_{n} = acc_{n}^2$.
Either case, the decoding is:
\begin{equation}
i_n = C\left( acc_{n} \cdot \left( acc_{n} \oplus \Sigma \right) \right) \oplus i_n'
\end{equation}

Yet this scheme has a huge weakness: it is impossible to encrypt the program if cycles are present in the control flow.
E.g. a loop's first instruction has two predecessors $acc_{n}^1$ and $acc_{n}^2$ where $acc_{n}^2$ is the state of the program at the end of the loop.
Then it becomes infeasible to compute $acc_{n}^2$: it depends on $acc_{n}^1 \cdot acc_{n}^2$.
The self-reference cannot be solved, since in this case it would violate \gls{HMAC} security requirements: one should not be able to find a preimage given an output.

So this scheme works only if the control flow graph is a \gls{DAG} which is very limiting in real life scenarios.
Instead two solutions (A and B) are proposed below with different security implications developed in section~\ref{sec:security}.

\paragraph{$p$-predecessors, solution A}
\label{sec:npredecessorsA}

It is possible to generalize in order to allow up to $p$ predecessors for an instruction and for a control flow with cycles.

In order to allow cycles, we must ``rebase'' our program state for all instructions having several predecessors. In this case, the program state is a new uniformly random value (noted $r$ below).
The problem is now to map valid predecessor states to this same new state.

Let $r$ be a random value in $\mathbb{F}_{2^b}$. Let $acc_n^i, i \in [\![ 1, p ]\!]$ be the allowed previous accumulator values for current instruction $i_n$.
A polynomial $P$ can be defined such that $\forall i \in [\![ 1, p ]\!], P(acc_n^i) = r$ using Lagrange interpolation.
Since the generated polynomial is minimal, it is constant if we do not define an additional point.
$P(0) = d$ for $d$ a random value (different than $r$) in $\mathbb{F}_{2^b}$.

The $p$ coefficients of $P$ are stored as program metadata with the corresponding instruction $i_n$.
At packing, \sname{} encrypts with $i_n' = C(r) \oplus i_n$.
To decrypt instruction $i_n$, we use $acc_n = P(acc_{n-1})$.
Note that the polynomial evaluation replaces the HMAC call.


\paragraph{$p$-predecessors, solution B}
\label{sec:npredecessorsB}

$\mathbb{F}_{2^b}$ can be decomposed in different subgroups $\mu_p$ where 
\begin{equation}
    \forall x \in \mu_p, x^p = 1
\end{equation}
(subgroup of $p$th-root of unity).
Such subgroups exist for all $p$ such that $p \mid 2^{b} - 1$.

For all valid $p$ (depending on $b$), we can define a scheme that allows $p$ predecessors. For example, $b=16$ allows a scheme with 5 predecessors ($p=5$ divides $65535=2^{16}-1$).

Let $acc_n^i, i \in [\![ 1, p ]\!]$ be the allowed previous accumulator values for current instruction $i_n$.
Let $r$ be a random value in $\mathbb{F}_{2^b}$.
Let $m \in \mu_p$ be a generator value for the subgroup.
We construct a polynomial $P$ (in $\mathbb{F}_{2^b}$) using Lagrange interpolation such that $\forall i \in [\![ 1, p ]\!], P(acc_n^i) = r \cdot m^i$.
The $p-1$ coefficients of $P$ are stored as program metadata with the corresponding instruction $i_n$.
At packing, \sname{} encrypts with $i_n' = C(r^p) \oplus i_n$.
To decrypt instruction $i_n$, we use $acc_n = P(acc_{n-1})^p$.
Indeed, by construction
\begin{equation}
    \forall i \in [\![ 1, p ]\!], P(acc_n^i)^p = \left(r \cdot  m^i \right)^p = r^p \cdot \left( m^p \right)^i = r^p.
\end{equation}

In this scheme, polynomials have degree $p-1$ instead of $p$ in solution A: the memory overhead is lower.

\subsubsection{Ensuring Instruction Integrity}
\label{sec:instruction_integrity}

To check the \gls{II}, it is enough to check an $acc_n$ against a truth value pre-generated at packing time.
The more frequent the check, the sooner a tempering is detected but the bigger is the required metadata.

A second strategy is to have valid instructions forming a set $I_v \subset \mathbb{F}_{2^w}$.
If $card(I_v) << card(\mathbb{F}_{2^w})$ a wrongly decoded instruction will have a very low probability of belonging to $I_v$, of being valid.

\subsection{Key management}

In this scheme, the component responsible for managing the secret key $k$ is critical.
In most cases, the binary encryption cannot be performed at compilation on the binary provider machine since it would requires to ship the (then shared) secret along with the binary.
As a consequence, any \gls{ISR} scheme using a secret key must have a packing phase that transform an unmodified binary (or extended with the \gls{CFG} information) into a hardened one.

The only other possibility is for the binary provider to encrypt the application for each intended recipient, then to use public-key cryptography to share the corresponding secret key with the targeted hardware.


\subsection{Limitations}

Apart from the performances overhead, our solution has severe limitations.
Since the \gls{CFG} must be perfectly known at packing time, indirect jumps and calls should be avoided.
In particular, the scheme is not compatible with virtual method tables required for runtime polymorphism in several languages (C++, java, \ldots).
Additionally, the scheme is tailored for self-contained applications. If the program must call external code (shared library, OS system call), things do not play well.
How to lift these limits should be investigated by the community.

% Dynamic dispatch ?
% Shared Library ?
% Dynamic Library Loading (aka plugins) ?
% Relocation ?
% Indirect branch ?

% Information recombination from shared shares.
% 1) a_1, a_2
% 2) a_2, a_3