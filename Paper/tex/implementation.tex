\section{Implementation}
\label{sec:implementation}

In order to test \sname{}, we implement it by modifying a CHIP-8 virtual machine to run hardened programs.
The sources for the reference and the hardened implementations can be found at \url{https://gitlab.inria.fr/rlasherm/HAPEI}. Licenses are MIT for software and CC-BY-4.0 for non-software.

\subsection{CHIP-8}

CHIP-8 is an \acrfull{ISA} initially intended to be run in a virtual machine on 8-bits microcomputers (from the 1970s).
Its purpose is to run the same video games on different hardware.
It is a good candidate for a hardened implementation because of its simplicity: $35$ instructions with only $1$ indirect branch instruction.
Binaries (called roms in the video game emulation tradition) can be freely found on the internet.
Additionally, its age means that it can easily be run on any modern computer, even with additional cryptographic computations, in real-time.

Our goal is therefore to run these roms in a hardened virtual machine. A simulated fault injection process, a key press modifies the next opcode by a randomly valid one, must be detected.
In order to validate the hardened implementation, we compare it to a reference implementation (without the hardening) and compare the behaviours.

The implementation is modularized: \textbf{chip8lib} contains all common structure definitions and the machinery to parse opcodes into instructions (sum type values).
\textbf{chip8ref} is the reference implementation, able to run, display and interact with emulated video games.
\textbf{chip8hard} is the hardened implementation, it packs the current rom at startup then executes its encrypted version according to the scheme presented in this paper (solution A).

\subsection{Reference implementation}

The two implementations have been done in the rust language. Rust has great performances and allows a simple representation of the virtual machine by using sum types.
The implementation has been inspired by the previous work at~\cite{noauthor_mike_nodate}, but modularized to factor code between the reference and hardened implementations.
The virtual machine is a 8-bit machine (word size) with 16-bit addresses.

\subsection{Hardened implementation}

\subsubsection{Packing}

The hardened implementation must pack the application before execution. This step requires a precise control flow graph extraction. 
This extraction is done in a classical way.
First we define a method \textit{cfg\_next} that given an instruction, its address and the call stack (stack to keep track of routine calls and returns) return all addresses that can possibly be executed next (and update the call stack).
Then starting from the first address, we recursively call \textit{cfg\_next} on the next instruction for all possible call stacks.
Meaning that if the next instruction has already been included in the \gls{CFG} previously but the call stack is different than the one during the previous \gls{CFG} inclusion, we continue the analysis with the new call stack.

The difficulty lies in indirect branches that make \gls{CFG} extraction difficult.
In the CHIP-8, there is only one such instruction \verb|JP V0, addr| that jumps to address \verb|addr| plus the content of register \verb|V0| (8-bit register).
In our \gls{CFG} extraction, we consider that the possible successors for this instruction are all addresses between \verb|addr| and \verb|addr| + 255.
Fortunately, all roms do not use this instruction.

Once the \gls{CFG} has been extracted, we compute all accumulator values (program states), polynomials and finally encrypt our instructions in the following order:
\begin{enumerate}
    \item $acc_{init}$ from IV.
    \item For all multi-predecessors instructions, draw a new random accumulator value. $acc_{init}$ is a predecessor for the entry instruction.
    \item Compute recursively all remaining accumulator values.
    \item Compute and store polynomials for all multi-predecessors instructions.
    \item For all instructions, encrypt it using corresponding accumulator value.
    \item Delete all accumulator values, we have to compute them on-the-fly at execution.
\end{enumerate}

\subsubsection{Execution}


At execution, the binary is already encrypted. At each tick of the virtual machine, the following actions are performed in order to execute instruction $i_n$ at address $PC$:
\begin{enumerate}
    \item Is there a polynomial $P$ associated with address $PC$ ?
    \item If yes, then its a multi-predecessors case: update the accumulator state $acc = P(acc)$.
    \item If there is no polynomial, then simply update the accumulator with the \gls{HMAC} function: $acc = HMAC_k(acc||i_{previous})$
    \item Then decrypt the instruction to be executed: $i_n = i_n' \oplus C(acc)$.
    \item If $i_n$ is valid we can execute it, in the other case we are under attack.
\end{enumerate}

Only one accumulator value must be remembered throughout the computation, lowering the cost of our solution.
This cost is both a big performance hit due to the on-the-fly decryption and accumulator update, and a memory overhead required to store the polynomials.
Since our implementation is a virtual machine, the performance overhead cannot be meaningfully measured.
But the memory overhead can be precisely measured as shown on table~\ref{tab:memory_usage}.

\begin{table}[h!]
    \caption{Hardening memory usage for a set of CHIP-8 roms found in~\cite{noauthor_chip-8_nodate} (solution A).}
    \label{tab:memory_usage}
    \begin{small}
\begin{tabular}{ | >{\centering\arraybackslash}p{2cm} | >{\centering\arraybackslash}p{1.4cm} | >{\centering\arraybackslash}p{1.8cm} | >{\centering\arraybackslash}p{1.8cm} | >{\centering\arraybackslash}p{1.4cm} | >{\centering\arraybackslash}p{1.9cm} | }
\hline
ROM name & ROM byte size & Instructions count & Polynomials count & Field elements count & Polynomials byte size (128-bit) \\
\hline
INVADERS & 1283 & 202 & 28 & 99 & 1584 \\
GUESS & 148 & 49 & 8 & 25 & 400 \\
KALEID & 120 & 59 & 10 & 32 & 512 \\
CONNECT4 & 194 & 67 & 5 & 19 & 304 \\
WIPEOFF & 206 & 101 & 15 & 47 & 752 \\
PONG2 & 264 & 126 & 19 & 60 & 960 \\
15PUZZLE & 384 & 116 & 17 & 54 & 864 \\
TETRIS & 494 & 189 & 32 & 106 & 1696 \\
BLINKY & 2356 & 856 & 84 & 310 & 4960 \\
VBRIX & 507 & 218 & 27 & 93 & 1488 \\
SYZYGY & 946 & 414 & 44 & 149 & 2384 \\
BRIX & 280 & 134 & 17 & 57 & 912 \\
TICTAC & 486 & 194 & 23 & 89 & 1424 \\
MAZE & 34 & 13 & 3 & 10 & 160 \\
PUZZLE & 184 & 87 & 10 & 34 & 544 \\
BLITZ & 391 & 121 & 15 & 47 & 752 \\
VERS & 230 & 103 & 24 & 73 & 1168 \\
PONG & 246 & 117 & 18 & 57 & 912 \\
UFO & 224 & 106 & 15 & 48 & 768 \\
TANK & 560 & 236 & 42 & 139 & 2224 \\
MISSILE & 180 & 75 & 12 & 37 & 592 \\
HIDDEN & 850 & 258 & 24 & 81 & 1296 \\
MERLIN & 345 & 124 & 13 & 45 & 720 \\
\hline
\end{tabular}
\end{small}
\end{table}

In this table, the hardening is performed for a set of binaries found in~\cite{noauthor_chip-8_nodate}.
We can see that the memory requirements at the 128-bit security level (size of one field element) is important: more memory is required to store the polynomials than the initial binary size.

Additionally, the roms are run in the reference virtual machine and in the hardened virtual machine to confirm functional equivalence. Then a simulated hardware fault injection mechanism is inserted.
When a specific key is pressed, the next opcode is replaced in memory with a random valid opcode.
On the reference implementation, the results of this fault injection is unpredictable: strange patterns are displayed on screen, nothing happen, another game screen is unlocked or we get a crash.
In the hardened machine, the fault injection means that all subsequent instructions will be wrongly decoded: a crash always follows because of an invalid instruction value.