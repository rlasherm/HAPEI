\section{Security Assessment}
\label{sec:security}

In this section, the security of the solution is analysed. As with most equivalent schemes, the details are critical.
In section~\ref{sec:scheme_security}, we discuss about the security of the proposed schemes.
In section~\ref{sec:diff_attacks}, we analyse the security problems due to the use of a stream cipher and how to overcome it.
Finally, in section~\ref{sec:multi_successors}, the limits of the \acrfull{PEI} guarantees are shown.

\subsection{Scheme security}
\label{sec:scheme_security}

The scheme security relies on the secrecy of the key stream, the accumulator values must remain secret.
Can the attacker gain information on one accumulator value, given she knows the encrypted instructions, the clear instructions (in the most advantageous case for her) and the polynomials ?
If she learn a given $acc_{n-1}$, then no information is gained on $acc_n$ without the knowledge of the secret key $k$ per the cryptographic properties of the \gls{HMAC}.

First, she can deduce $C(acc_n) = i'_n \oplus i_n$. If $C$ is a cryptographically secure hash function, then no information is gained on $acc_n$.
Lower constraints on $C$ are possible, since we only care about the correct preimage security: the attacker must find the correct preimage, not just a satisfying one.

\paragraph{$p$-predecessors, solution A}

Let $i_n$ be a $p$-predecessors instruction: 
\begin{equation}
\exists P \in \mathbb{F}_{2^b}\left[X\right] | \forall i \in [\![1, p]\!], P(acc_n^i) = r
\end{equation}
with $r$ a random value.
$P$ is a public non-constant polynomial but all $acc_n^i$ and $r$ are secret.

Knowing $P$, the attacker cannot find any $acc_n^i$ nor $r$: $r$ can be any value in $\mathbb{F}_{2^b}$ and for most $r$ she can find corresponding valid $acc_n^i$.
Yet if she learn $r$, then finding the roots of $P(X)-r$ is trivial. If she learn a given $acc_n^i$, then she can compute $r=P(acc_n^i)$, then find the other accumulator values.
As a consequence, a polynomial links all corresponding secrets together. If one value is discovered, all the others are too.

A same accumulator value can be used as a legitimate input to several polynomials.
Yet the resulting systems of equation are always underdetermined. There is one unknown per polynomial corresponding to the random $r$ value, plus at least $1$ secret $acc_n^i$ value.
But there again, in this case all secrets are linked: discovering one may mean discovering the others.

The problem with solution A is that $P$ is constructed in a very specific way: Lagrange interpolation ensures that $P(X)-r$ has $p$ distinct roots ($P$ has degree $p$), the maximum possible.
The attacker can use the peculiarity to gain information on $r$ and $acc_n^i, \forall i$.
Given a random polynomial $Q$ of degree $p$, the probability that $Q$ has $p$ distinct roots corresponds to the number of combinations to distribute $p$ roots over $2^b$ values divided by the total number of polynomials of degree $p$.
\begin{equation}
    \frac{\binom{2^b}{p}}{\left(2^b\right)^{p+1}}.
    \label{eq:highrootcount_proportion}
\end{equation}

Since in our case, $p << 2^b$, equation~\eqref{eq:highrootcount_proportion} becomes
\begin{equation}
    \frac{1}{p! \left(2^b\right)^{p+1}}.
\end{equation}

As a conclusion, a proportion of $\frac{1}{p!}$ random polynomials have $p$ roots. The greater the $p$, the better for the attacker that becomes able to discriminate $r$.
In most cases, $p$ is low and $2^b$ is high ($b \geq 128$) so the security should not be compromised since the attacker cannot possibly enumerate all possible $r$.

\paragraph{$p$-predecessors, solution B}

This possibility for the attacker to discriminate $r$ given $P$ is the main motivation for the alternative solution B.
In this proposition, $r$ is not a special value with respect to $P$: $P(X)-r\cdot m^i$ may have any number of roots ($\geq 1$).
But then, it means that additional roots may be considered valid program states.
Some random illegitimate accumulator values could be mapped to the legitimate one.
Since the attacker should not be able to control the accumulator value, the security is not compromised.

Finally, the choice between solutions A and B depends on the attacker model: if she can control $acc_n$, then solution A must be chosen.
If not but she has a huge computation power, solution B should be preferred.

\subsection{Differential Attack}
\label{sec:diff_attacks}

If the attacker is able to see the plain/decrypted instructions (or deduce from observed behaviour), she can execute one arbitrary instruction $i_a$.

\begin{equation}
    i_n' \oplus e = C\left( acc_n \right) \oplus i_n \oplus e
\end{equation}

To execute $i_a$, simply choose $e = i_n \oplus i_a$.
But the next state of the program is
$$acc_{n+1} = HMAC_k(acc_n || i_a)$$
which is unpredictable for the attacker by the required properties of $HMAC_k$.
This attack is present in all schemes using a key stream (xoring a secret data with the text). 

The mitigation is to wait for $i_{n+1}$ valid decryption before executing $i_n$.
In this case, if the attacker tries to force execution of $i_a$ instead of $i_n$, \gls{II} detects the bad behaviour (cf section~\ref{sec:instruction_integrity}).

\subsection{Multi-successors attack}
\label{sec:multi_successors}

In the proposed scheme, several instructions can have the same associated program state. An example is given in listing~\ref{lst:multi_successors}.

\begin{lstlisting}[caption={A pseudo assembly program},label=lst:multi_successors]
    i0: CMP R0, #0 // Compare register R0 with 0
    i1: BEQ 3 // Go to i3 if equal
    i2: JUMP 4 // Go to i4
    i3: ...
\end{lstlisting}

In this example, the possible transitions from instruction $i_1$ are $i_1 \Rightarrow i_2$ or $i_1 \Rightarrow i_3$. $i_1$ has two successors but both $i_2$ and $i_3$ have one predecessor.
As a consequence, $acc_2 = acc_3$ and encrypted instructions differential is conserved: $i_2' \oplus i_3' = i_2 \oplus i_3$.

The attacker can switch these instructions at will and they will be correctly decoded.
A mitigation would require to includes a unique identifier in the accumulator update formula:
\begin{equation}
    acc_{n+1} = HMAC_k(acc_n || i_n || n).
\end{equation}

But such an attack does not break \acrlong{PEI}: the execution where the attacker switches the instructions is indistinguishable from a legitimate execution apart from instruction addresses.
The program is semantically correct.
And if the next instructions do not correspond to a legitimate program execution, they cannot be correctly decrypted.
In conclusion, this attack illustrates the limits of the \gls{PEI} guarantees.
The Program Counter $PC$ is part of the processor state: \gls{StI} is the guarantee that should prevent this attack.