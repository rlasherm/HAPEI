\section{Context}
\label{sec:context}

In order to specify and verify our proposal, we must precisely define the capacities of the attacker and the integrity guarantees we provide.

\paragraph{Attacker models}
Several models are considered:
\begin{itemize}
    \item \gls{CIA}: an attacker tries to divert the control flow to execute its own malicious payload.
    \item \gls{CRA}: an attacker tries to execute a malicious payload composed by a sequence of legitimate pieces of programs (often called widgets in Return-Oriented Programming).
    \item \gls{HFAoI}: the attacker can edit the program, at runtime, by modifying instruction values.
    \item \gls{HFAoD}: the attacker can edit the program, at runtime, by modifying data values.
\end{itemize}

\paragraph{Integrities}
To protect against these attacks, the execution of the software must enforce guarantees:
\begin{itemize}
    \item \gls{CFI}: the control flow cannot be modified (no arbitrary jumps). The control flow follows the valid \gls{CFG}.
    \item \gls{II}: the instruction values must not be altered.
    \item \gls{DI}: the data handled by the program must not be altered.
    \item \gls{StI}: the processor state (configuration, registers, program counter, \ldots) must not be altered.
\end{itemize}

Often, \gls{DI}, \gls{II} and \gls{StI} are considered together under the name of \gls{SI}.
Here we call \gls{PEI} the combination of \gls{CFI} and \gls{II}.

Our scheme, \sname{}, ensures \gls{PEI} in order to protect against \gls{CIA}, \gls{CRA} and \gls{HFAoI}. 
Yet to be complete, a solution should also ensure \gls{DI}. In our opinion, one of the best solution would be to encrypt all data with one secret key per application.
Since in the following we consider only one application executing, we suppose that data integrity is guaranteed at a higher level.

\gls{StI} is probably the most difficult to guarantee in presence of an attacker with hardware fault injection means.
It is also very implementation specific, we discuss in section~\ref{sec:multi_successors} how an attack can be achieved on our implementation because \gls{StI} is not guaranteed.

If numerous works discuss how to ensure integrities, most consider only \gls{CIA} and \gls{CRA} attacker models. 
Yet, hardware fault attacks are a reality and must be mitigated. Unfortunately, \gls{HFAoI} is a much more powerful attacker model and most previous schemes do not protect against it (cf section~\ref{sec:previous_works}).