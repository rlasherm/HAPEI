\section{Introduction}
\label{sec:intro}

In order to ensure the security of an application, developers have to do every thing they can to reduce the number of bugs that could lead to vulnerabilities.
But for the most critical applications, software must be proven correct.
Yet one bug missed and an attacker can, in some cases, execute arbitrary code.
Moreover, this bug can be absent in the binary but created at runtime with a hardware fault injection, breaking software proofs assumptions.

\paragraph{Motivation}

The problem is illustrated below with a simple example.

\begin{lstlisting}[language=C,caption={A simple loop in C.},label=lst:cloop]
int count = 0;
for(int i = 0; i < 100; i++) {
    count++;
}
\end{lstlisting}

The assembly code corresponding with the loop in listing~\ref{lst:cloop} can be seen in listing~\ref{lst:aloop}.

\begin{lstlisting}[language={[x86masm]Assembler},caption={The same loop in x86 assembly.},label=lst:aloop]
movl	$0, -8(%rbp) // count = 0
movl	$0, -4(%rbp) // i = 0
jmp	.L2
.L3:
addl	$1, -8(%rbp) // count++
addl	$1, -4(%rbp) // i++
.L2:
cmpl	$99, -4(%rbp) // compare i and 99
jle	.L3 // if i <= 99, jump to .L3 else continue
\end{lstlisting}


In this case the program execution must ensure:
\begin{itemize}
    \item instructions are executed in order,
    \item it is not possible to jump to an arbitrary instruction of the loop. Only the ``landing'' instructions can be jumped to (first ones after L2 or L3).
    \item When executing a ``landing'' instruction, the previous state of the program must be correct. E.g. the program state is one of the two authorized ones (a proper definition of program state is given in section~\ref{sec:solution}). This implies that the jumps are all legitimate.
    \item No instruction can be overwritten, no instruction can be skipped.
\end{itemize}

These guarantees must be valid even if the attacker is able to arbitrarily modify instructions at runtime. In this case, execution must stop to prevent further damages.

\paragraph{Contribution}
This work proposes \sname{} to ensure that the intended software is what is actually running on the chip.
Inspired by SOFIA~\cite{ruan_de_clercq_sofia:_2016}, the solution is a hardware \acrfull{ISR} scheme that ensures \acrfull{II} and \acrfull{CFI}, even in the case of a \acrfull{HFAoI}.
We demonstrate that we can harden a binary without any modification in the compilation chain with a CHIP-8 virtual machine implementation.
It means that the \gls{ISA} does not have to change in the compiler's view: \sname{} is transparent at the software level.
During application installation (also called packing), the instructions are encrypted. The encryption scheme encodes the authorized program states and the transitions from one state to the next (effectively encoding the \gls{CFG}).
During execution, instructions are decrypted on-the-fly. The decryption is correct only if the program state is correct and the control flow graph is followed.

\paragraph{Organization}
In this paper, we start with the context and the necessary definitions in section~\ref{sec:context}.
After a review of similar relevant works in section~\ref{sec:previous_works}, the theory behind \sname{} is presented in section~\ref{sec:solution}.
A discussion on the security of the scheme follows in section~\ref{sec:security}.
We propose an implementation detailed in section~\ref{sec:implementation} and finally our conclusion is drawn in section~\ref{sec:conclusion}.