/*
 * File: main.rs
 * Project: src
 * Created Date: Tuesday June 29th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 2nd July 2018 5:11:47 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[macro_use] extern crate failure;
extern crate chip8lib;

use chip8lib::cfg::CFG;

use failure::Error;

use std::env;
use std::fs::File;
use std::path::Path;
use std::io::Read;

use chip8lib::core::*;

const START_ADDRESS_USIZE: usize = 0x200;
const START_ADDRESS: MemoryAddress = MemoryAddress(START_ADDRESS_USIZE as u16);

fn load_program<T: AsRef<Path>>(path: T, memory: &mut [u8]) -> Result<(), Error> {
    //load file
    let mut file = File::open(path.as_ref()).map_err(|e|format_err!("Error while loading file @ {}: {}", path.as_ref().display(), e))?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).map_err(|e|format_err!("Cannot read file {}: {}", path.as_ref().display(), e))?;

    //write memory
    for (i, b) in buffer.iter().enumerate() {
        memory[i + START_ADDRESS_USIZE] = *b;
    }

    Ok(())
}

fn main() -> Result<(), Error> {
    let mut memory: [u8; MEMORY_SIZE as usize] = [0; MEMORY_SIZE as usize];

    //loading program
    let args: Vec<String> = env::args().collect();
    let to_load = args.get(1).ok_or(format_err!("A binary path must be specified as an argument."))?;
    print!("Loading {} program...", to_load);
    load_program(to_load, &mut memory)?;
    println!("Done");

    print!("Disassemble...");
    let dis = CFG::disassemble(&memory, START_ADDRESS)?;
    println!("Done");

    print!("Writing Disassembly to file...");
    CFG::writedis2file(dis, "dis.list")?;
    println!("Done");


    print!("Exctracing CFG...");
    let cfg = CFG::extract(&memory, START_ADDRESS)?;
    println!("Done");

    print!("Writing CFG to file...");
    cfg.writecfg2file("cfg.list")?;
    println!("Done");

    Ok(())
}
