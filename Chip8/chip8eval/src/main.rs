/*
 * File: main.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 5:21:57 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[macro_use] extern crate failure;
extern crate rand;
extern crate chip8lib;
extern crate chip8ref;
extern crate chip8hard;

use chip8lib::core::*;

use std::env;
use std::fs;
use std::io::{BufWriter, Write, stdout};
use std::path::Path;

use failure::Error;



fn main() -> Result<(), Error> {

    let args: Vec<String> = env::args().collect();
    let mut machine = chip8hard::cpu::Cpu::new();

    let logfile = fs::File::create("logfile.txt")?;
    let mut buf_writer = BufWriter::new(logfile);

    //loading programs
    let folder = args.get(1).ok_or(format_err!("A path must be specified as an argument."))?;
    let rom_pathes = fs::read_dir(folder)?;

    write!(buf_writer, "ROM name & ROM size (bytes) & Instructions count & Polynomials count & Average polynomial degree & Field elements count & Polynomials size (128-bit field, bytes) \\\\\n")?;

    for rom_res in rom_pathes {
        if let Ok(rom) = rom_res {
            let rom_name = rom.file_name().into_string().map_err(|_|format_err!("Cannot disply rom name"))?.clone();
            let rom_path_str = rom.path().to_str().ok_or(format_err!("Cannot convert path"))?.to_string();
            let rom_path = Path::new(&rom_path_str);
            print!("Loading {} program...", rom_name);
            let _ = stdout().flush();
            machine.clear();
            let (rom_size, icount) = machine.load_program_file(rom_path).map_err(|e|format_err!("{}",e))?;

            let mem_stats = machine.memory_stats();
            let byte_sec_level = 16;
            let polynomials_byte_size = byte_sec_level * mem_stats.field_el_count;
            
            write!(buf_writer, "{} & {} & {} & {} & {} & {} \\\\\n",rom_name, rom_size, icount, mem_stats.poly_count, mem_stats.field_el_count, polynomials_byte_size)?;

            println!("Done");
            println!("{} & {} & {} & {} & {} & {} \\\\\n",rom_name, rom_size, icount, mem_stats.poly_count, mem_stats.field_el_count, polynomials_byte_size);
        }
    }

    


    
    Ok(())
}