/*
 * File: register_bank.rs
 * Project: cpu
 * Created Date: Friday June 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 8th June 2018 9:40:33 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use chip8lib::core::*;

use std::fmt;

#[derive(Debug)]
pub struct RegisterBank {
    bank: [u8; REGISTER_COUNT as usize]
}

impl RegisterBank {
    pub fn new() -> RegisterBank {
        RegisterBank { bank: [0; REGISTER_COUNT as usize] }
    }

    pub fn reset(&mut self) {
        self.bank = [0; REGISTER_COUNT as usize]
    }

    pub fn set(&mut self, index: RegisterIndex, val: u8) {
        self.bank[index.index()] = val;
    }

    pub fn get(&self, index: RegisterIndex) -> u8 {
        self.bank[index.index()]
    }
}

impl fmt::Display for RegisterBank {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.bank)
    }
}