/*
 * File: hardening.rs
 * Project: cpu
 * Created Date: Tuesday July 3rd 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 5:16:02 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use num_bigint::BigUint;
use num_traits::ToPrimitive;
use binary_fields::binary_field::*;

use ring::{digest, hmac, rand};
use ring::rand::SecureRandom;

// use failure::Error;

lazy_static! {
    static ref M128VAL: BigUint = {
        create_sparse_biguint(&vec![128, 7, 2, 1, 0])
    };

    static ref M192VAL: BigUint = {
        create_sparse_biguint(&vec![192, 7, 2, 1, 0])
    };

    static ref DIVISORS128: Vec<u64> = // these values divides 2^128 -1
                        vec![1, 
                            3,
                            5,
                            15,
                            17,
                            51,
                            85,
                            255,
                            257,
                            641,
                            771,
                            1285,
                            1923,
                            3205,
                            3855,
                            4369,
                            9615,
                            10897,
                            13107,
                            21845,
                            32691,
                            54485,
                            65535];

}

#[derive(Debug,PartialEq,Eq,Clone)]
pub struct M128 {}

impl ModuloPolynomial for M128 {
    fn modulo() -> &'static BigUint {
        &M128VAL
    }
}

#[derive(Debug,PartialEq,Eq,Clone,Hash)]
pub struct M192 {}

impl ModuloPolynomial for M192 {
    fn modulo() -> &'static BigUint {
        &M192VAL
    }
}

pub type F = M192;


pub fn gen_key() -> hmac::SigningKey {
    let mut key_val = [0u8; 32];
    let rng = rand::SystemRandom::new();
    rng.fill(&mut key_val).unwrap();

    hmac::SigningKey::new(&digest::SHA256, key_val.as_ref())
}

pub fn acc_update(key: &hmac::SigningKey, last_acc: &BinaryField<F>, last_opcode: u16) -> BinaryField<F> {

    let mut buffer = last_acc.get_bytes();
    buffer.push(((last_opcode >> 8) & 0xFF) as u8);
    buffer.push((last_opcode & 0xFF) as u8);
    let signature = hmac::sign(key, &buffer);

    BinaryField::<F>::from_bytes(signature.as_ref())
}

pub fn select_divisor128(predecessor_count: u64) -> Option<u64> {
    for d in DIVISORS128.iter() {
        if *d > predecessor_count {
            return Some(*d);
        }
    }
    None
}

pub fn compression_function(state: &BinaryField<F>) -> u16 {
    (state.get_val() % &<u16>::max_value()).to_u16().unwrap()
}