/*
 * File: main.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 11:51:07 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[macro_use] extern crate log;
#[macro_use] extern crate failure;
extern crate rand;
extern crate chip8lib;
extern crate binary_fields;
extern crate num_traits;
extern crate num_bigint;
#[macro_use] extern crate lazy_static;
extern crate ring;

pub mod cpu;
