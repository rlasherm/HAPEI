/*
 * File: main.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 13th September 2018 2:42:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

// #[macro_use] extern crate failure;
extern crate fern;
extern crate log;
extern crate rand;
extern crate chip8lib;
extern crate chip8ref;
extern crate chip8hard;
extern crate ggez;
extern crate sdl2;

use chip8lib::core::*;
use chip8lib::io::*;

use ggez::*;
// use ggez::graphics::Color;
use ggez::event::{self, Keycode};

use sdl2::keyboard::Scancode;

use std::env;
use std::fs;
use std::path::Path;

struct MainState {
    emulator: Box<Machine>,
}

impl MainState {
    fn new(_ctx: &mut Context, emulator: Box<Machine>) -> GameResult<MainState> {
        let s = MainState { emulator };
        Ok(s)
    }
}

fn sdlscan2key(scan: Scancode) -> Option<Key> {
        match scan {
            Scancode::Up    => Some(Key(2)),
            Scancode::Left  => Some(Key(4)),
            Scancode::Right => Some(Key(6)),
            Scancode::Down  => Some(Key(8)),

            Scancode::Num7 => Some(Key(1)),
            Scancode::Num8 => Some(Key(2)),
            Scancode::Num9 => Some(Key(3)),
            Scancode::Num0 => Some(Key(0xC)),

            Scancode::U => Some(Key(4)),
            Scancode::I => Some(Key(5)),
            Scancode::O => Some(Key(6)),
            Scancode::P => Some(Key(0xD)),

            Scancode::J         => Some(Key(7)),
            Scancode::K         => Some(Key(8)),
            Scancode::L         => Some(Key(9)),
            Scancode::Semicolon => Some(Key(0xE)),

            Scancode::M         => Some(Key(0xA)),
            Scancode::Comma     => Some(Key(0)),
            Scancode::Period    => Some(Key(0xB)),
            Scancode::Slash     => Some(Key(0xF)),

            _ => None
        }
    }

const DEBUG: bool = false;

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        if !DEBUG {
            self.emulator.cycle().map_err(|e|format!("{}",e))?;
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        // graphics::set_color(ctx, Color::from_rgb(20, 20, 20));
        // graphics::clear(ctx);
        self.emulator.display().display(ctx)?;
        // graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: Keycode, _keymod: event::Mod, _repeat: bool) {
        if let Some(scan) = Scancode::from_keycode(keycode) {
            if let Some(key) = sdlscan2key(scan) {
                self.emulator.keypad().press_key(key);
            }
            else {
                match scan {
                    Scancode::Q => { //fault injection
                        println!("Fault injection !");
                        // self.emulator.inject_fault(0x8013);
                        //force valid opcode
                        let fault_opcode: u16 = 
                        {
                            let mut op = rand::random();
                            loop {
                                if Chip8Instruction::parse_instruction(op).is_ok() {
                                    break;
                                } 
                                else {
                                    op = rand::random();
                                }
                            }
                            op
                        };

                        self.emulator.inject_fault(fault_opcode);
                    },
                    _ => {}
                }
            }
        }
    }

    fn key_up_event(&mut self, ctx: &mut Context, keycode: Keycode, _keymod: event::Mod, _repeat: bool) {
        if keycode == Keycode::Escape {
            ctx.quit().unwrap();
        }

        //debug
        if DEBUG {
            if keycode == Keycode::B {
                self.emulator.cycle().unwrap();
            }
        }

        if let Some(scan) = Scancode::from_keycode(keycode) {
            if let Some(key) = sdlscan2key(scan) {
                self.emulator.keypad().release_key(key);
            }
        }
    }

}

fn main() -> GameResult<()> {

    let log_level_stdout =
    // log::LevelFilter::Error;
    // log::LevelFilter::Warn;
    // log::LevelFilter::Info;
    // log::LevelFilter::Debug;
    // log::LevelFilter::Trace;
    log::LevelFilter::Off;
    

    let log_level_log = 
    // log::LevelFilter::Error;
    // log::LevelFilter::Warn;
    // log::LevelFilter::Info;
    // log::LevelFilter::Debug;
    log::LevelFilter::Trace;
    // log::LevelFilter::Off;


    //configure logger
    let log_filename = format!("out.log");

    //remove file if exists
    let _ = fs::remove_file(&log_filename);

    //setup logging
    //stdout level depends on verbosity
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        out.finish(format_args!("[{}] {}",
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
                        .level(log_level_stdout)
                        .chain(std::io::stdout());

    let file_log = fern::Dispatch::new()
                        .level(log_level_log)
                        .chain(fern::log_file(log_filename).map_err(|e|format!("{}", e))?);

    base_config.chain(file_log)
                .chain(stdout_log)
                .apply()
                .map_err(|e|format!("{}", e))?;


    let args: Vec<String> = env::args().collect();
    //load options
    let options = if let Some(opts) = args.get(2) {
        opts.clone()
    } 
    else {
        String::new()
    };
    let hardmachine = options.contains("h");
    

    //init emulator
    print!("Building emulator...");
    let mut machine: Box<Machine> = if hardmachine {
        print!("hardened...");
        Box::new(chip8hard::cpu::Cpu::new())
    }
    else {
        print!("ref...");
        Box::new(chip8ref::cpu::Cpu::new())
    };
    // let mut machine = chip8ref::cpu::Cpu::new();

    println!("Done");

    //loading program
    let to_load = args.get(1).ok_or(format!("A binary path must be specified as an argument."))?;
    print!("Loading {} program...", to_load);
    let path_to_load = Path::new(to_load);
    machine.load_program_file(path_to_load).map_err(|e|format!("{}",e))?;
    machine.reset();
    println!("Done");


    //init 2D engine (ggez)
    print!("Initializing 2D engine...");
    let cb = ContextBuilder::new("chip8", "Ronan")
        .window_setup(conf::WindowSetup::default().title("CHIP-8 emulator"))
        .window_mode(conf::WindowMode::default().dimensions(
                                                    (DISPLAY_WIDTH*DISPLAY_SCALE) as u32, 
                                                    (DISPLAY_HEIGHT*DISPLAY_SCALE) as u32));


    let ctx = &mut cb.build()?;
    let state = &mut MainState::new(ctx, machine)?;
    println!("Done");

    //run 2D engine
    print!("Running...");

    event::run(ctx, state)?;
    println!("Done");
    
    Ok(())
}