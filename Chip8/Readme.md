# CHIP-8

Use chip8run to launch a binary on a virtual machine. Tested on Ubuntu 18.04.

## Building

Install the stable [rust toolchain](https://rustup.rs/).
Go to the *chip8run* folder.

Build with
```bash
cargo build --release
```

## Run

The binary can be found in *./target/release/*.

Launch the reference implementation with
```bash
chip8run PATH2BINARY
```

Launch the reference implementation with
```bash
chip8run PATH2BINARY h
```

## Inject a fault

Inject a fault with the first key of your keyboard (e.g. 'Q' on a QWERTY keyboard).