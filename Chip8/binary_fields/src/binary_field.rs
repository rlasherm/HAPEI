/*
 * File: binary_field.rs
 * Project: src
 * Created Date: Friday June 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 5:15:37 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use num_traits::*;
use num_bigint::{BigUint, ParseBigIntError};
use num_bigint::RandBigInt;

use std::ops::*;
use std::fmt;
use std::marker::PhantomData;

use rand;

/// Dependant type like
pub trait ModuloPolynomial: Clone {
    fn modulo() -> &'static BigUint;
}

#[derive(Debug,Clone,Eq, PartialOrd,Ord,Hash)]
pub struct BinaryField<MOD: ModuloPolynomial> {
    val: BigUint,
    _phantom: PhantomData<MOD>,
}

impl<MOD: ModuloPolynomial> PartialEq for BinaryField<MOD> {
    fn eq(&self, other: &BinaryField<MOD>) -> bool {
        self.val == other.val
    }
}

impl<MOD: ModuloPolynomial> Zero for BinaryField<MOD> {
    fn zero() -> Self {
        BinaryField { val: BigUint::zero(), _phantom: PhantomData }
    }

    fn is_zero(&self) -> bool {
        self.val.is_zero()
    }
}

impl<MOD: ModuloPolynomial> One for BinaryField<MOD> {
    fn one() -> Self {
        BinaryField { val: BigUint::one(), _phantom: PhantomData }
    }

    fn is_one(&self) -> bool {
        self.val.is_one()
    }
}


impl<MOD: ModuloPolynomial> Add for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn add(self, other: BinaryField<MOD>) -> BinaryField<MOD> {
        let res = self.val ^ other.val;
        BinaryField { val: res, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Add<&'b BinaryField<MOD>> for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn add(self, other: &'b BinaryField<MOD>) -> BinaryField<MOD> {
        let res = self.val ^ &other.val;
        BinaryField { val: res, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Sub<&'b BinaryField<MOD>> for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn sub(self, other: &'b BinaryField<MOD>) -> BinaryField<MOD> {
        let res = self.val ^ &other.val;
        BinaryField { val: res, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Sub for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn sub(self, other: BinaryField<MOD>) -> BinaryField<MOD> {
        let res = self.val ^ other.val;
        BinaryField { val: res, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Mul for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn mul(self, other: BinaryField<MOD>) -> BinaryField<MOD> {
        self.multiply_reduce(&other)
    }
}

impl<'b, MOD: ModuloPolynomial> Mul<&'b BinaryField<MOD>> for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn mul(self, other: &'b BinaryField<MOD>) -> BinaryField<MOD> {
        self.multiply_reduce(other)
    }
}

impl<MOD: ModuloPolynomial> Div for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn div(self, other: BinaryField<MOD>) -> BinaryField<MOD> {
        self * Self::inv(other)
    }
}

impl<'b, MOD: ModuloPolynomial> Div<&'b BinaryField<MOD>> for BinaryField<MOD> {
    type Output = BinaryField<MOD>;

    fn div(self, other: &'b BinaryField<MOD>) -> BinaryField<MOD> {
        let cloned: BinaryField<MOD> = other.clone();
        self * Self::inv(cloned)
    }
}

pub fn create_sparse_biguint(coeffs: &[usize]) -> BigUint {
    let mut r = BigUint::zero();
    for c in coeffs.iter() {
        r += BigUint::one() << *c;
    }
    r
}

impl<MOD: ModuloPolynomial> fmt::Display for BinaryField<MOD> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:x}", self.val)
    }
}

impl<MOD: ModuloPolynomial> BinaryField<MOD> {

    pub fn new(val: BigUint) -> BinaryField<MOD> {
        BinaryField::<MOD>::reduce(BinaryField { val: val, _phantom: PhantomData })
    }

    pub fn from_hex(hex: &str) -> Result<BinaryField<MOD>, ParseBigIntError> {
        Ok(BinaryField::<MOD>::new(BigUint::from_str_radix(hex, 16)?))
    }

    pub fn from_bytes(bytes: &[u8]) -> BinaryField<MOD> {
        BinaryField::<MOD>::new(BigUint::from_bytes_le(bytes))
    }

    pub fn random() -> BinaryField<MOD> {
        let mut rng = rand::thread_rng();
        let big = rng.gen_biguint(MOD::modulo().bits());
        let mut bf = BinaryField { val: big, _phantom: PhantomData };
        bf = Self::reduce(bf);
        bf
    }

    pub fn memory_bitsize() -> usize {
        MOD::modulo().bits()
    }

    pub fn get_val(&self) -> &BigUint {
        &self.val
    }

    pub fn get_bytes(&self) -> Vec<u8> {
        self.val.to_bytes_le()
    }

    fn binary_multiply(a: &BigUint, b: &BigUint) -> BigUint {
        let mut res = BigUint::zero();
        let size = a.bits();
        let mut tmpb = BinaryField::<MOD>::reduce_bui(b.clone());
        let mut tmpa = BinaryField::<MOD>::reduce_bui(a.clone());


        for _ in 0..size {

            if &tmpa & BigUint::one() == BigUint::one() {
                res ^= &tmpb;
            }
            tmpa >>= 1;

            tmpb <<= 1;
            tmpb = BinaryField::<MOD>::quick_reduce(tmpb);
        }

        assert!(res.bits() < MOD::modulo().bits());
        res
    }


    fn multiply_reduce(&self, other: &BinaryField<MOD>) -> BinaryField<MOD> {
        let res = BinaryField::<MOD>::binary_multiply(&self.val, &other.val);
        BinaryField { val: res, _phantom: PhantomData}
    }

    pub fn pow(self, exp: u64) -> BinaryField<MOD> {
        //square and multiply
        let mut acc = BigUint::one();
        let fact = self.val;
        let tmp_exp = exp;

        for i in (0..64).rev() {
            acc = Self::binary_multiply(&acc, &acc);
            if (tmp_exp >> i) & 1 == 1 {
                acc = Self::binary_multiply(&acc, &fact);
            }
        }

        BinaryField { val: acc, _phantom: PhantomData }
    }

    fn quick_reduce(val: BigUint) -> BigUint {
        if val.bits() == MOD::modulo().bits() {
            val ^ MOD::modulo()
        }
        else {
            val
        }

    }

    fn reduce_bui(big_ui: BigUint) -> BigUint {
        let mod_size = MOD::modulo().bits() - 1;
        let max_size = 2*mod_size - 2;
        assert!(big_ui.bits() <= max_size + 1, "{} is not < {}", big_ui.bits(), max_size);

        let r = MOD::modulo() ^ BigUint::one() << (MOD::modulo().bits()-1);
        let w = 64;

        let mut c = BigUint::zero();
        let one = BigUint::one();

        for i in (mod_size..=max_size).rev() {
            if (&big_ui >> i) & &one == one {

                let j = (i-mod_size)/w;
                let k = (i-mod_size) - w*j;

                c = c ^ (&r << (k + j));
            }
        }

        for i in 0..mod_size {
            c = c ^ (&big_ui & (BigUint::one() << i));
        }

        c
    }

    fn mod_reduce(&mut self) {
        let mod_size = MOD::modulo().bits() - 1;
        let max_size = 2*mod_size - 2;
        assert!(self.val.bits() <= max_size + 1);

        let r = MOD::modulo() ^ BigUint::one() << (MOD::modulo().bits()-1);
        let w = 64;

        let mut c = BigUint::zero();
        let one = BigUint::one();

        for i in (mod_size..=max_size).rev() {
            if (&self.val >> i) & &one == one {

                let j = (i-mod_size)/w;
                let k = (i-mod_size) - w*j;

                c = c ^ (&r << (k + j));
            }
        }

        for i in 0..mod_size {
            c = c ^ (&self.val & (BigUint::one() << i));
        }

        self.val = c;
    }

    pub fn reduce(mut self) -> BinaryField<MOD> {
        self.mod_reduce();
        self
    }

    pub fn inv(self) -> BinaryField<MOD> {
        //self is accumulator
        let mut acc = BigUint::one();
        let mut b = Self::binary_multiply(&self.val, &self.val);

        for _ in 0..MOD::modulo().bits()-2 {
            acc = Self::binary_multiply(&acc, &b);//multiply
            b = Self::binary_multiply(&b, &b);//square
        }

        BinaryField { val: acc, _phantom: PhantomData }
    }
}

#[test]
fn test_identities() {

    lazy_static! {
        static ref M1VAL: BigUint = {
            BigUint::from_i64(0x80000000).unwrap()
        };
    }

    #[derive(Debug, Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }


    let zero: BinaryField<M1> = BinaryField::zero();
    assert!(zero.is_zero());
}

#[test]
fn test_field() {

    lazy_static! {
        static ref M2VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };

        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![4, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M2 {}

    impl ModuloPolynomial for M2 {
        fn modulo() -> &'static BigUint {
            &M2VAL
        }
    }

    //mul
    let a1 = BinaryField::<M1>::new(BigUint::from_str_radix("4", 16).unwrap());
    let b1 = BinaryField::<M1>::new(BigUint::from_str_radix("a", 16).unwrap());

    let c1 = a1*b1;
    let c1_verif = BinaryField::<M1>::new(BigUint::from_str_radix("e", 16).unwrap());
    assert_eq!(c1, c1_verif, "Multiplication 1");


    let a2 = BinaryField::<M2>::new(BigUint::from_str_radix("2ab6d61ffa4ee0bee1f860f4686a43e2", 16).unwrap());
    let b2 = BinaryField::<M2>::new(BigUint::from_str_radix("5c943875504e3c1d5102588919a5caa8", 16).unwrap());

    let c2 = a2*b2;
    let c2_verif = BinaryField::<M2>::new(BigUint::from_str_radix("298b4683eb15e5c3ee79d506546ffc69", 16).unwrap());
    assert_eq!(c2, c2_verif, "Multiplication 2");


    //inv
    let r = BinaryField::<M2>::new(BigUint::from_str_radix("2ab6d61ffa4ee0bee1f860f4686a43e2", 16).unwrap());
    // let r2 = r.clone();
    let invr = r.inv();
    // let mulr = r2*&invr;
    let invr_verif = BinaryField::<M2>::new(BigUint::from_str_radix("b28a30045ae2b3c9c4d7f95b400a0906", 16).unwrap());
    assert_eq!(invr, invr_verif, "Inversion");

    //div
    let a3 = BinaryField::<M2>::new(BigUint::from_str_radix("f82dec06844ae86fad8fabf33050bc70", 16).unwrap());
    let b3 = BinaryField::<M2>::new(BigUint::from_str_radix("da2bbe6a53d450593577da055dd81c73", 16).unwrap());

    let c3 = a3/b3;
    let c3_verif = BinaryField::<M2>::new(BigUint::from_str_radix("312a88cd24f5487292ed59184351dfb7", 16).unwrap());
    assert_eq!(c3, c3_verif, "Division");
}


#[test]
fn test_pow() {

    lazy_static! {
        static ref M2VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };

        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![4, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M2 {}

    impl ModuloPolynomial for M2 {
        fn modulo() -> &'static BigUint {
            &M2VAL
        }
    }

    let r = BinaryField::<M2>::new(BigUint::from_str_radix("3d9ce8efcc4664223f62f475a047b165", 16).unwrap());
    let r2 = r.clone().pow(33);
    let r3 = r.clone().pow(2);

    let r2_verif = BinaryField::<M2>::new(BigUint::from_str_radix("adfdd742c3d4cc37811d757cd3f588f3", 16).unwrap());
    let r3_verif = BinaryField::<M2>::from_hex("b7437a9fd8fb9213ddb86a7321730b03").unwrap();
    assert_eq!(r2, r2_verif);
    assert_eq!(r3, r3_verif);
}

#[test]
fn test_mod_reduce() {
    lazy_static! {
        static ref M2VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };

        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![4, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M2 {}

    impl ModuloPolynomial for M2 {
        fn modulo() -> &'static BigUint {
            &M2VAL
        }
    }

    let mut r = BinaryField::<M1>::from_hex("4a").unwrap();
    r.mod_reduce();

    let r_verif = BinaryField::<M1>::from_hex("6").unwrap();
    assert_eq!(r, r_verif);


    let mut r2 = BinaryField::<M2>::from_hex("1234567890123456789012345678901234567890").unwrap();
    r2.mod_reduce();

    let r2_verif = BinaryField::<M2>::from_hex("90123456789012345678901b50f0e7f8").unwrap();
    assert_eq!(r2, r2_verif);
}