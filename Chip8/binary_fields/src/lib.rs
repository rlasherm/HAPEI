/*
 * File: binary_field.rs
 * Project: src
 * Created Date: Friday June 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 3rd July 2018 11:34:41 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

extern crate num_traits;
extern crate num_bigint;
extern crate rand;

#[allow(unused_imports)]
#[macro_use] extern crate lazy_static;

pub mod binary_field;
pub mod polynomial;

pub use binary_field::{BinaryField, ModuloPolynomial};
pub use polynomial::Polynomial;