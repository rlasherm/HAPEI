/*
 * File: polynomial.rs
 * Project: src
 * Created Date: Tuesday June 26th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 25th July 2018 10:40:03 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use std::collections::BTreeMap;
use std::marker::PhantomData;
use std::ops::*;
use std::fmt;

use num_traits::*;

use binary_field::*;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct Polynomial<MOD: ModuloPolynomial> {
    coeffs: BTreeMap<u64, BinaryField<MOD>>,//(degree, coeff)
    _phantom: PhantomData<MOD>
}

impl<MOD: ModuloPolynomial> Polynomial<MOD> {
    pub fn new(coeffs: BTreeMap<u64, BinaryField<MOD>>) -> Polynomial<MOD> {
        Polynomial { coeffs, _phantom: PhantomData }
    }

    pub fn x() -> Polynomial<MOD> {
        let mut coeffs = BTreeMap::new();
        coeffs.insert(1, BinaryField::<MOD>::one());
        Polynomial { coeffs , _phantom: PhantomData }
    }

    pub fn memory_bitsize(&self) -> usize {
        self.coeffs.len() * BinaryField::<MOD>::memory_bitsize()
    }

    pub fn degree(&self) -> u64 {
        let mut max = 0;
        for (d, _) in self.coeffs.iter() {
            if *d > max {
                max = *d;
            }
        }
        max
    }

    pub fn evaluate(&self, x: &BinaryField<MOD>) -> BinaryField<MOD> {
        let mut acc = BinaryField::<MOD>::zero();

        for (deg, coef) in self.coeffs.iter() {
            let powed = x.clone().pow(*deg);
            acc = acc + (powed * coef);
        }

        acc
    }

    pub fn lagrange_interpolation(xs: &Vec<BinaryField<MOD>>, ys: &Vec<BinaryField<MOD>>) -> Polynomial<MOD> {
        assert_eq!(xs.len(), ys.len());

        let mut res = Polynomial::<MOD>::zero();

        for (j, y) in ys.iter().enumerate() {
            res = res + Polynomial::<MOD>::lagrange_basis(xs, j) * y;
        }

        res
    }

    fn lagrange_basis(xs: &Vec<BinaryField<MOD>>, j: usize) -> Polynomial<MOD> {
        //product all xs apart from j
        let mut res = Polynomial::<MOD>::one();

        // let zero = BinaryField::<MOD>::zero();
        let xj = xs.get(j).unwrap();

        for (i, x) in xs.iter().enumerate() {
            if i != j {
                res = res * (Self::x() - x) / (xj.clone() - x);
            }
        }
        res
    }
}

impl<MOD: ModuloPolynomial> Zero for Polynomial<MOD> {
    fn zero() -> Self {
        Polynomial { coeffs: BTreeMap::new(), _phantom: PhantomData }
    }

    fn is_zero(&self) -> bool {
        self.coeffs.is_empty()
    }
}

impl<MOD: ModuloPolynomial> One for Polynomial<MOD> {

    //1
    fn one() -> Self {
        let mut coeffs = BTreeMap::new();
        coeffs.insert(0, BinaryField::<MOD>::one());
        Polynomial { coeffs , _phantom: PhantomData }
    }

    fn is_one(&self) -> bool {
        

        if let Some(c1) = self.coeffs.get(&0) {
            self.coeffs.len() == 1 && *c1 == BinaryField::<MOD>::one()
        }
        else {
            false
        }
    }
}

impl<MOD: ModuloPolynomial> Add for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn add(self, other: Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d, c) in self.coeffs.iter() {
            new_coeffs.insert(*d, c.clone());
        }

        for (d, c) in other.coeffs.iter() {
            let prev = new_coeffs.remove(d).unwrap_or(BinaryField::<MOD>::zero());
            new_coeffs.insert(*d, prev + c);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Add<&'b Polynomial<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn add(self, other: &'b Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d, c) in self.coeffs.iter() {
            new_coeffs.insert(*d, c.clone());
        }

        for (d, c) in other.coeffs.iter() {
            let prev = new_coeffs.remove(d).unwrap_or(BinaryField::<MOD>::zero());
            new_coeffs.insert(*d, prev + c);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Sub for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn sub(self, other: Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d, c) in self.coeffs.iter() {
            new_coeffs.insert(*d, c.clone());
        }

        for (d, c) in other.coeffs.iter() {
            let prev = new_coeffs.remove(d).unwrap_or(BinaryField::<MOD>::zero());
            new_coeffs.insert(*d, prev - c);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Sub<&'b BinaryField<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn sub(self, other: &'b BinaryField<MOD>) -> Polynomial<MOD> {
        let other_poly = Self::one() * other;
        self - other_poly
    }
}



impl<'b, MOD: ModuloPolynomial> Sub<&'b Polynomial<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn sub(self, other: &'b Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d, c) in self.coeffs.iter() {
            new_coeffs.insert(*d, c.clone());
        }

        for (d, c) in other.coeffs.iter() {
            let prev = new_coeffs.remove(d).unwrap_or(BinaryField::<MOD>::zero());
            new_coeffs.insert(*d, prev - c);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Mul<&'b Polynomial<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn mul(self, other: &'b Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            for (d2, c2) in other.coeffs.iter() {
                let newd = d1+d2;
                let prev = new_coeffs.remove(&newd).unwrap_or(BinaryField::<MOD>::zero());

                new_coeffs.insert(newd, prev + (c1.clone()*c2));
            }
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Mul for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn mul(self, other: Polynomial<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            for (d2, c2) in other.coeffs.iter() {
                let newd = d1+d2;
                let prev = new_coeffs.remove(&newd).unwrap_or(BinaryField::<MOD>::zero());

                new_coeffs.insert(newd, prev + (c1.clone()*c2));
            }
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Mul<BinaryField<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn mul(self, other: BinaryField<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            new_coeffs.insert(*d1, c1.clone()*&other);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Mul<&'b BinaryField<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn mul(self, other: &'b BinaryField<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            new_coeffs.insert(*d1, c1.clone()*other);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> Div<BinaryField<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn div(self, other: BinaryField<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            new_coeffs.insert(*d1, c1.clone()/&other);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<'b, MOD: ModuloPolynomial> Div<&'b BinaryField<MOD>> for Polynomial<MOD> {
    type Output = Polynomial<MOD>;

    fn div(self, other: &'b BinaryField<MOD>) -> Polynomial<MOD> {
        let mut new_coeffs: BTreeMap<u64, BinaryField<MOD>> = BTreeMap::new();

        for (d1, c1) in self.coeffs.iter() {
            new_coeffs.insert(*d1, c1.clone()/other);
        }

        Polynomial { coeffs: new_coeffs, _phantom: PhantomData }
    }
}

impl<MOD: ModuloPolynomial> fmt::Display for Polynomial<MOD> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let poly_str = self.coeffs.iter().map(|(d,c)| format!("{}*X^{}", c, d)).collect::<Vec<String>>().join(" + ");
        write!(f, "{}",poly_str)
    }
}

#[test]
fn test_polynomial() {
    use num_bigint::BigUint;

    lazy_static! {
        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    let mut map: BTreeMap<u64, BinaryField<M1>> = BTreeMap::new();
    map.insert(0, BinaryField::from_hex("fb9b34dd3da9c27c0b02cad3323ccb89").unwrap());
    map.insert(1, BinaryField::from_hex("1ca9d12b980fed664bb2ddf7defdd4e7").unwrap());
    map.insert(2, BinaryField::from_hex("a715f92505b38b995e8716c19aca7789").unwrap());

    let r: BinaryField<M1> = BinaryField::from_hex("f36b56a8ff975e013d246ca25ca8b171").unwrap();

    let poly: Polynomial<M1> = Polynomial::new(map);
    let evaluated = poly.evaluate(&r);
    let verif: BinaryField<M1> = BinaryField::from_hex("a0f6c4546207132408ab2c295c625469").unwrap();
    assert_eq!(evaluated, verif);
}

#[test]
fn test_poly_mul() {
    use num_bigint::BigUint;

    lazy_static! {
        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    let one = Polynomial::<M1>::one();
    let x = Polynomial::<M1>::x();

    let xbis = one * &x;

    assert_eq!(x, xbis);
}

#[test]
fn test_interpolation() {
    use num_bigint::BigUint;

    lazy_static! {
        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    let mut map: BTreeMap<u64, BinaryField<M1>> = BTreeMap::new();
    map.insert(0, BinaryField::from_hex("2637799cab00c7d3c84f46788c4caa68").unwrap());
    map.insert(1, BinaryField::from_hex("4e69f6936f472ea83033488d95d54119").unwrap());
    let poly_verif: Polynomial<M1> = Polynomial::new(map);

    let xs: Vec<BinaryField<M1>> = vec![BinaryField::from_hex("fcd44fd178cd8971e7ef555eb3264c80").unwrap(), BinaryField::from_hex("7bd08b5178664af9ee8e5cd5d62aa7a").unwrap()];
    let ys: Vec<BinaryField<M1>> = vec![BinaryField::from_hex("7a90cdf6571da4645299701dfb761b5b").unwrap(), BinaryField::from_hex("97d0bc302f4f53ebc2a60f8b6146a660").unwrap()];

    let poly = Polynomial::<M1>::lagrange_interpolation(&xs, &ys);
    assert_eq!(poly, poly_verif);

    for (x, y) in xs.iter().zip(ys) {
        assert_eq!(y, poly.evaluate(x));
    }
}

#[test]
fn test_random_interpolation() {
    use num_bigint::BigUint;

    lazy_static! {
        static ref M1VAL: BigUint = {
            create_sparse_biguint(&vec![128, 7, 2, 1, 0])
        };
    }

    #[derive(Debug,PartialEq,Eq,Clone)]
    pub struct M1 {}

    impl ModuloPolynomial for M1 {
        fn modulo() -> &'static BigUint {
            &M1VAL
        }
    }

    let count = 3;

    let mut xs: Vec<BinaryField<M1>> = Vec::new();
    let mut ys: Vec<BinaryField<M1>> = Vec::new();

    for _ in 0..count {
        xs.push(BinaryField::random());
        ys.push(BinaryField::random());
    }

    let poly = Polynomial::<M1>::lagrange_interpolation(&xs, &ys);
    for (x, y) in xs.iter().zip(ys) {
        assert_eq!(y, poly.evaluate(x));
    }
}