/*
 * File: timer.rs
 * Project: src
 * Created Date: Thursday June 7th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 7th June 2018 2:36:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */
use std::time::*;

#[derive(Debug)]
pub struct Stopwatch {
    last_tick: Instant
}


impl Stopwatch {
    pub fn start() -> Stopwatch {
        Stopwatch { last_tick: Instant::now() }
    }

    pub fn reset(&mut self) {
        self.tick(); //forget result
    }

    pub fn tick(&mut self) -> Duration {
        let now = Instant::now();
        let elapsed = now.duration_since(self.last_tick);
        self.last_tick = now;
        elapsed
    }
}

#[test]
fn test_timing_resolution() {
    let i1 = Instant::now();
    let i2 = Instant::now();

    let duration = i2.duration_since(i1);
    println!("Duration = {:?}", duration);
}