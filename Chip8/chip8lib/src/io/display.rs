/*
 * File: display.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 7th June 2018 3:33:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

pub const DISPLAY_WIDTH: usize = 64;
pub const DISPLAY_HEIGHT: usize = 32;
pub const DISPLAY_SCALE: usize = 16;

use ggez::*;
use ggez::graphics::{Mesh, MeshBuilder, Point2, DrawMode, Color};

#[derive(Debug)]
pub struct Display {
    rows: [u64; 32], //each value is a row (64*32 monochrome screen)
    need_draw: bool,

    //ggez stuff
    mesh: Option<Mesh>,
    front_color: Color,
    back_color: Color
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct DisplayLocation {
    x: usize,
    y: usize
}

impl DisplayLocation {
    pub fn xy(x: usize, y: usize) -> DisplayLocation {
        DisplayLocation { x, y}
    }

    pub fn tuple(self) -> (usize, usize) {
        (self.x, self.y)
    }
}

impl Display {
    pub fn new() -> Display {
        Display { 
            rows: [0; 32], 
            need_draw: true, 
            mesh: None,
            front_color: Color::from_rgb(223, 154, 8),
            back_color: Color::from_rgb(47, 37, 71)
            }
    }

    fn init_mesh(&mut self, ctx: &mut Context) -> GameResult<()> {
        let mesh = MeshBuilder::new()
                    .polygon(DrawMode::Fill,
                            &[
                                Point2::new(0f32, 0f32),//top left
                                Point2::new(DISPLAY_SCALE as f32, 0f32),//top right
                                Point2::new(DISPLAY_SCALE as f32, DISPLAY_SCALE as f32),//bottom right
                                Point2::new(0f32, DISPLAY_SCALE as f32),//bottom left
                            ])
                    .build(ctx)?;

        self.mesh = Some(mesh);

        Ok(())
    }

    // fn rowcol2ind(row: usize, col: usize) -> usize {
    //     row * DISPLAY_WIDTH + col
    // }

    pub fn clear(&mut self) {
        for r in self.rows.iter_mut() {
            *r = 0;
        }
        self.need_draw = true;
    }

    /// Draw a sprite [sprite] starting at location [loc]
    /// Display is toroidal
    /// Return true if collision
    pub fn draw_sprite(&mut self, loc: DisplayLocation, sprite: &[u8]) -> bool {
        let mut collision = false;
        let (mut start_x, mut display_row) = loc.tuple();

        // println!("Draw @({}, {})", start_x, display_row);

        start_x %= DISPLAY_WIDTH;
        display_row %= DISPLAY_HEIGHT;

        for sprite_row in sprite.iter() {
            // compute how sprite should be set
            let update = ((*sprite_row as u64) << 56).rotate_right(start_x as u32);

            // update screen
            let previous_state = self.rows[display_row];
            let new_state = previous_state ^ update;
            self.rows[display_row] = new_state;

            // update row in a toroidal way
            display_row = (display_row + 1) % DISPLAY_HEIGHT;

            // detect collision
            // if bit of previous = 1 and same update bit = 1 then collision
            collision |= (previous_state & update) != 0;
        }

        self.need_draw = true;
        collision
    }

    pub fn display(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.mesh.is_none() {
            self.init_mesh(ctx)?;
        }

        if self.need_draw {
            graphics::set_color(ctx, Color::from_rgb(20, 20, 20))?;
            graphics::clear(ctx);
            self.need_draw = false;

            if let Some(ref mesh) = self.mesh {
                for (r, row) in self.rows.iter().enumerate() {
                    for c in 0..DISPLAY_WIDTH {
                        let x = (c*DISPLAY_SCALE) as f32;
                        let y = (r*DISPLAY_SCALE) as f32;

                        let pixel: bool = (row & (0x8000000000000000 >> c)) != 0;
                        
                        if pixel {
                            //set color front
                            graphics::set_color(ctx, self.front_color)?;
                            graphics::draw(ctx, mesh, Point2::new(x,y), 0.0)?;
                        }
                        else {
                            //set color back
                            graphics::set_color(ctx, self.back_color)?;
                            graphics::draw(ctx, mesh, Point2::new(x,y), 0.0)?;
                        }
                    }
                }
            }
            graphics::present(ctx);
        }
        
        Ok(())
    }
}