/*
 * File: keypad.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 6th June 2018 11:33:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Key(pub u8);

impl Key {
    pub fn from_str(k: &str) -> Result<Key, Error> {
        match k .to_lowercase().as_str() {
            "0" => Ok(Key(0)),
            "1" => Ok(Key(1)),
            "2" => Ok(Key(2)),
            "3" => Ok(Key(3)),
            "4" => Ok(Key(4)),
            "5" => Ok(Key(5)),
            "6" => Ok(Key(6)),
            "7" => Ok(Key(7)),
            "8" => Ok(Key(8)),
            "9" => Ok(Key(9)),
            "a" => Ok(Key(10)),
            "b" => Ok(Key(11)),
            "c" => Ok(Key(12)),
            "d" => Ok(Key(13)),
            "e" => Ok(Key(14)),
            "f" => Ok(Key(15)),

            "up" => Ok(Key(2)),
            "down" => Ok(Key(8)),
            "left" => Ok(Key(4)),
            "right" => Ok(Key(6)),


            _ => Err(format_err!("Incorrect key string {}", k))
        }
    }

    pub fn from_val(val: u8) -> Result<Key, Error> {
        if val < 16 {
            Ok(Key(val))
        }
        else {
            Err(format_err!("Incorrect key {}", val))
        }
    }

    pub fn index(&self) -> usize {
        self.0 as usize
    }
}

#[derive(Debug)]
pub struct Keypad {
    keys: [bool; 16] //are keys pressed ?
}

impl Keypad {
    pub fn new() -> Keypad {
        Keypad { keys: [false; 16] }
    }

    pub fn reset(&mut self) {
        for k in self.keys.iter_mut() {
            *k = false;
        }
    }

    pub fn press_key(&mut self, key: Key) {
        self.keys[key.0 as usize] = true;
    }

    pub fn release_key(&mut self, key: Key) {
        self.keys[key.0 as usize] = false;
    }

    pub fn pressed(&self, key: Key) -> bool {
        self.keys[key.index()]
    }

    pub fn first_pressed(&self) -> Option<Key> {
        for (i, k) in self.keys.iter().enumerate() {
            if *k == true {
                return Some(Key(i as u8));
            }
        }
        None
    }
}