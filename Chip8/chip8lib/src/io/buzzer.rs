/*
 * File: buzzer.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 6th June 2018 11:29:43 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[derive(Debug)]
pub struct Buzzer {
    buzz: bool
}

impl Buzzer {
    pub fn new() -> Buzzer {
        Buzzer { buzz: false }
    }

    pub fn reset(&mut self) {
        self.stop_buzz();
    }

    pub fn start_buzz(&mut self) {
        self.buzz = true;
    }

    pub fn stop_buzz(&mut self) {
        self.buzz = false;
    }
}