/*
 * File: register.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 8th June 2018 9:38:30 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;

use std::fmt;

pub const REGISTER_COUNT: u8 = 16;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct RegisterIndex(pub u8);

pub const V0: RegisterIndex = RegisterIndex(0);
pub const VF: RegisterIndex = RegisterIndex(15);

impl RegisterIndex {
    pub fn from_name(name: &str) -> Result<RegisterIndex, Error> {
        match name {
            "V0" => Ok(RegisterIndex(0)),
            "V1" => Ok(RegisterIndex(1)),
            "V2" => Ok(RegisterIndex(2)),
            "V3" => Ok(RegisterIndex(3)),
            "V4" => Ok(RegisterIndex(4)),
            "V5" => Ok(RegisterIndex(5)),
            "V6" => Ok(RegisterIndex(6)),
            "V7" => Ok(RegisterIndex(7)),
            "V8" => Ok(RegisterIndex(8)),
            "V9" => Ok(RegisterIndex(9)),
            "VA" => Ok(RegisterIndex(10)),
            "VB" => Ok(RegisterIndex(11)),
            "VC" => Ok(RegisterIndex(12)),
            "VD" => Ok(RegisterIndex(13)),
            "VE" => Ok(RegisterIndex(14)),
            "VF" => Ok(RegisterIndex(15)),
            _ => Err(format_err!("Unknown register name: {}", name))
        }
    }

    pub fn from_val(val: u8) -> Result<RegisterIndex, Error> {
        if val < REGISTER_COUNT {
            Ok(RegisterIndex(val))
        }
        else {
            Err(format_err!("Forbidden register index: {}", val))
        }
    }

    pub fn index(&self) -> usize {
        self.0 as usize
    }
}

impl fmt::Display for RegisterIndex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "V{}", self.0)
    }
}