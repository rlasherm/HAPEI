/*
 * File: timed_register.rs
 * Project: chip8lib
 * Created Date: Thursday June 7th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 7th June 2018 11:21:48 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use std::time::Duration;

const NSTICKS_60HZ: u64 = 16666700;


#[derive(Debug)]
pub struct TimedRegister8 {
    remaining_ns: u64
}

impl TimedRegister8 {
    pub fn new() -> TimedRegister8 {
        TimedRegister8 { remaining_ns: 0}
    }

    pub fn reset(&mut self) {
        self.remaining_ns = 0;
    }

    pub fn set(&mut self, new_val: u8) {
        self.remaining_ns = (new_val as u64) * NSTICKS_60HZ;
    }

    pub fn get(&self) -> u8 {
        TimedRegister8::get_timer_val_from_remaining_time(self.remaining_ns)
    }

    fn get_timer_val_from_remaining_time(remaining_ns: u64) -> u8 {
        if remaining_ns == 0 {
            0
        }
        else {
            ((remaining_ns / NSTICKS_60HZ) + 1) as u8
        }
    }

    /// Update this timer depending on time and return true if terminated (=0)
    pub fn tick(&mut self, elapsed: Duration) -> bool {
        let elaps_ns = elapsed.as_secs() * 1_000_000_000 + elapsed.subsec_nanos() as u64;

        if self.remaining_ns > elaps_ns { //still going
            self.remaining_ns -= elaps_ns;
            false
        }
        else {//terminated
            self.remaining_ns = 0;
            true
        }
    }

    pub fn terminated(&self) -> bool {
        self.remaining_ns == 0
    }
}