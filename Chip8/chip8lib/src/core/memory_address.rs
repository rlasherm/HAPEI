/*
 * File: memory_address.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 25th July 2018 11:36:42 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;

use std::fmt;

pub const ENTRY_VAL: u16 = 65535;
pub const MEMORY_SIZE: u16 = 4096;
pub const INST_SIZE: u16 = 2;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct MemoryAddress(pub u16);


impl MemoryAddress {
    pub fn from_val(add: u16) -> Result<MemoryAddress, Error> {
        if add < MEMORY_SIZE {
            Ok(MemoryAddress(add))
        }
        else {
            Err(format_err!("Forbidden memory address: {} >= {}", add, MEMORY_SIZE))
        }
    }

    pub fn entry() -> MemoryAddress {
        MemoryAddress(ENTRY_VAL)
    }

    pub fn is_entry(&self) -> bool {
        self.0 == ENTRY_VAL
    }

    pub fn index(&self) -> usize {
        self.0 as usize
    }

    pub fn next_inst(&mut self) -> Result<(), Error> {
        self.next_multiple_inst(1)
    }

    // move the address by i intructions
    pub fn next_multiple_inst(&mut self, i: u16) -> Result<(), Error> {
        self.offset(i*INST_SIZE)
    }

    pub fn offset(&mut self, off: u16) -> Result<(), Error> {
        if self.0 + off < MEMORY_SIZE {
            self.0 += off;
            Ok(())
        }
        else {
            Err(format_err!("Memory address out of range ({:x})", self.0 + off))
        }
    }
}

impl fmt::Display for MemoryAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}