/*
 * File: isa.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 3rd July 2018 9:11:59 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;

use std::fmt;
use std::collections::HashSet;

use core::*;


/// Instruction set from http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Chip8Instruction {
    /// Clear the display.
    CLS,

    /// Return from a subroutine.
    RET,

    // Jump to a machine code routine at address
    // This instruction is only used on the old computers on which Chip-8 was originally implemented. It is ignored by modern interpreters.
    // SYS(MemoryAddress),

    /// Jump to address
    JP(MemoryAddress),

    /// Call subroutine at address
    CALL(MemoryAddress),

    /// Skip next instruction if Vx = kk. (immediate)
    SEi(RegisterIndex,u8),

    /// Skip next instruction if Vx = Vy.
    SE(RegisterIndex, RegisterIndex),

    /// Skip next instruction if Vx != kk. (immediate)
    SNEi(RegisterIndex, u8),

    /// Skip next instruction if Vx != Vy.
    SNE(RegisterIndex, RegisterIndex),

    /// Set Vx = kk. (immediate)
    LDi(RegisterIndex,u8),

    /// Set Vx = Vx + kk. (immediate)
    ADDi(RegisterIndex,u8),

    /// Set Vx = Vy.
    MOV(RegisterIndex,RegisterIndex),

    /// Set Vx = Vx OR Vy.
    OR(RegisterIndex, RegisterIndex),

    /// Set Vx = Vx AND Vy.
    AND(RegisterIndex, RegisterIndex),

    /// Set Vx = Vx XOR Vy.
    XOR(RegisterIndex, RegisterIndex),

    /// Set Vx = Vx + Vy, set VF = carry.
    ADD(RegisterIndex, RegisterIndex),

    /// Set Vx = Vx - Vy, set VF = NOT borrow.
    SUB(RegisterIndex, RegisterIndex),

    /// Set Vx = Vy - Vx, set VF = NOT borrow.
    SUBN(RegisterIndex,RegisterIndex),

    /// Set Vx = Vy >> 1.
    SHR(RegisterIndex,RegisterIndex),

    /// Set Vx = Vy << 1.
    SHL(RegisterIndex,RegisterIndex),

    /// Set I = address.
    LDI(MemoryAddress),

    /// Jump to location address + V0.
    JPV0Off(MemoryAddress),

    /// Set Vx = random byte AND kk.
    RND(RegisterIndex,u8),

    /// Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
    DRW(RegisterIndex,RegisterIndex, u8 /*nibble*/),

    /// Skip next instruction if key with the value of Vx is pressed.
    SKP(RegisterIndex),

    /// Skip next instruction if key with the value of Vx is not pressed.
    SKNP(RegisterIndex),

    /// Set Vx = delay timer value.
    LDDT2reg(RegisterIndex),

    /// Wait for a key press, store the value of the key in Vx.
    LDKey(RegisterIndex),

    /// Set delay timer = Vx.
    LDreg2DT(RegisterIndex),

    /// Set sound timer = Vx.
    LDreg2ST(RegisterIndex),

    ///Set I = I + Vx.
    ADDI(RegisterIndex),

    /// Set I = location of sprite for digit Vx. The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx. 
    LDFont(RegisterIndex),

    /// Store BCD representation of Vx in memory locations I, I+1, and I+2. 
    /// The interpreter takes the decimal value of Vx, and places the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.
    LDBCB(RegisterIndex),

    /// Store registers V0 through Vx in memory starting at location I.
    PUSHI(RegisterIndex),

    /// Read registers V0 through Vx from memory starting at location I.
    POPI(RegisterIndex)
}


impl Chip8Instruction {
    pub fn parse_instruction(opcode: u16) -> Result<Chip8Instruction, Error> {
        match opcode & 0xF000 {
            0x0000 => Chip8Instruction::parse_0x0xxx(opcode),
            0x1000 => Chip8Instruction::parse_0x1xxx(opcode),
            0x2000 => Chip8Instruction::parse_0x2xxx(opcode),
            0x3000 => Chip8Instruction::parse_0x3xxx(opcode),
            0x4000 => Chip8Instruction::parse_0x4xxx(opcode),
            0x5000 => Chip8Instruction::parse_0x5xxx(opcode),
            0x6000 => Chip8Instruction::parse_0x6xxx(opcode),
            0x7000 => Chip8Instruction::parse_0x7xxx(opcode),
            0x8000 => Chip8Instruction::parse_0x8xxx(opcode),
            0x9000 => Chip8Instruction::parse_0x9xxx(opcode),
            0xA000 => Chip8Instruction::parse_0xaxxx(opcode),
            0xB000 => Chip8Instruction::parse_0xbxxx(opcode),
            0xC000 => Chip8Instruction::parse_0xcxxx(opcode),
            0xD000 => Chip8Instruction::parse_0xdxxx(opcode),
            0xE000 => Chip8Instruction::parse_0xexxx(opcode),
            0xF000 => Chip8Instruction::parse_0xfxxx(opcode),
            _ => Err(format_err!("Unkown opcode {:x}", opcode))
        }
    }

    pub fn parse_instructions(opcodes: &Vec<u16>) -> Result<Vec<Chip8Instruction>, Error> {
        let mut result = Vec::new();

        for i in opcodes {
            result.push(Chip8Instruction::parse_instruction(*i)?);
        }

        Ok(result)
    }
    
    fn parse_0x0xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        match inst {
            0x00E0 => Ok(Chip8Instruction::CLS),
            0x00EE => Ok(Chip8Instruction::RET),
            _ => Err(format_err!("Unkown 0x0xxx instruction {:x}", inst))
        }
    }

    fn parse_0x1xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let add = MemoryAddress::from_val(inst & 0x0FFF)?;
        Ok(Chip8Instruction::JP(add))
    }

    fn parse_0x2xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let add = MemoryAddress::from_val(inst & 0x0FFF)?;
        Ok(Chip8Instruction::CALL(add))
    }

    fn parse_0x3xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let reg_index = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let imm = (inst & 0xFF) as u8;
        Ok(Chip8Instruction::SEi(reg_index, imm))
    }

    fn parse_0x4xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let reg_index = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let imm = (inst & 0xFF) as u8;
        Ok(Chip8Instruction::SNEi(reg_index, imm))
    }

    fn parse_0x5xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let vy = RegisterIndex::from_val(((inst & 0x00F0) >> 4) as u8)?;
        Ok(Chip8Instruction::SE(vx, vy))
    }

    fn parse_0x6xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let imm = (inst & 0xFF) as u8;
        Ok(Chip8Instruction::LDi(vx, imm))
    }

    fn parse_0x7xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let imm = (inst & 0xFF) as u8;
        Ok(Chip8Instruction::ADDi(vx, imm))
    }

    fn parse_0x8xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let vy = RegisterIndex::from_val(((inst & 0x00F0) >> 4) as u8)?;
        
        let op = inst & 0x000F;
        match op {
            0 => Ok(Chip8Instruction::MOV(vx,vy)),
            1 => Ok(Chip8Instruction::OR(vx,vy)),
            2 => Ok(Chip8Instruction::AND(vx,vy)),
            3 => Ok(Chip8Instruction::XOR(vx,vy)),
            4 => Ok(Chip8Instruction::ADD(vx,vy)),
            5 => Ok(Chip8Instruction::SUB(vx,vy)),
            6 => Ok(Chip8Instruction::SHR(vx,vy)),
            7 => Ok(Chip8Instruction::SUBN(vx,vy)),
            0xE => Ok(Chip8Instruction::SHL(vx,vy)),
            _ => Err(format_err!("Unkown 0x8xxx instruction {:x}", inst))
        }
    }

    fn parse_0x9xxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let vy = RegisterIndex::from_val(((inst & 0x00F0) >> 4) as u8)?;
        Ok(Chip8Instruction::SNE(vx, vy))
    }

    fn parse_0xaxxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let add = MemoryAddress::from_val(inst & 0x0FFF)?;
        Ok(Chip8Instruction::LDI(add))
    }

    fn parse_0xbxxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let add = MemoryAddress::from_val(inst & 0x0FFF)?;
        Ok(Chip8Instruction::JPV0Off(add))
    }

    fn parse_0xcxxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let imm = (inst & 0xFF) as u8;
        Ok(Chip8Instruction::RND(vx,imm))
    }

    fn parse_0xdxxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        let vy = RegisterIndex::from_val(((inst & 0x00F0) >> 4) as u8)?;
        let nibble = (inst & 0x0F) as u8;
        Ok(Chip8Instruction::DRW(vx,vy,nibble))
    }

    fn parse_0xexxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        
        let op = inst & 0x00FF;
        match op {
            0x9E => Ok(Chip8Instruction::SKP(vx)),
            0xA1 => Ok(Chip8Instruction::SKNP(vx)),
            _ => Err(format_err!("Unkown 0xExxx instruction {:x}", inst))
        }
    }

    fn parse_0xfxxx(inst: u16) -> Result<Chip8Instruction, Error> {
        let vx = RegisterIndex::from_val(((inst & 0x0F00) >> 8) as u8)?;
        
        let op = inst & 0x00FF;
        match op {
            0x07 => Ok(Chip8Instruction::LDDT2reg(vx)),
            0x0A => Ok(Chip8Instruction::LDKey(vx)),
            0x15 => Ok(Chip8Instruction::LDreg2DT(vx)),
            0x18 => Ok(Chip8Instruction::LDreg2ST(vx)),
            0x1E => Ok(Chip8Instruction::ADDI(vx)),
            0x29 => Ok(Chip8Instruction::LDFont(vx)),
            0x33 => Ok(Chip8Instruction::LDBCB(vx)),
            0x55 => Ok(Chip8Instruction::PUSHI(vx)),
            0x65 => Ok(Chip8Instruction::POPI(vx)),
            _ => Err(format_err!("Unkown 0xFxxx instruction {:x}", inst))
        }
    }

    fn several_next_addresses(current_address: MemoryAddress, count: usize) -> Result<HashSet<MemoryAddress>, Error> {
        let mut res = HashSet::new();
        let mut add_it = current_address;

        for _ in 0..count {
            add_it.next_inst()?;
            res.insert(add_it);
        }

        Ok(res)
    }

    fn this_address(current_address: MemoryAddress) -> Result<HashSet<MemoryAddress>, Error> {
        let mut res = HashSet::new();
        res.insert(current_address);
        Ok(res)
    }

    fn jpv0off_next_addresses(address: MemoryAddress) -> Result<HashSet<MemoryAddress>, Error> {
        let mut res = HashSet::new();
        let mut add_it = address;

        for _ in 0..256 {
            res.insert(add_it);
            add_it.next_inst()?;
        }

        Ok(res)
    }

    //For disassembly, we only need to explore all instructions once -> no stack
    pub fn dis_next(&self, current_address: MemoryAddress) -> Result<HashSet<MemoryAddress>, Error> {
        match self {
            Chip8Instruction::CLS                   => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::RET                   => {
                Ok(HashSet::new())
            },
            Chip8Instruction::JP(add)               => Chip8Instruction::this_address(*add),
            Chip8Instruction::CALL(add)             => {
                let mut res = HashSet::new();
                res.insert(*add);
                let mut next = current_address;
                next.next_inst()?;
                res.insert(next);
                Ok(res)
            },
            Chip8Instruction::SEi(_, _)             => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SE(_, _)              => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SNEi(_, _)            => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SNE(_ , _)            => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::LDi(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADDi(_, _)            => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::MOV(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::OR(_, _)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::AND(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::XOR(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADD(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SUB(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SUBN(_, _)            => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SHR(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SHL(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDI(_)                => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::JPV0Off(add)          => Chip8Instruction::jpv0off_next_addresses(*add),
            Chip8Instruction::RND(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::DRW(_, _, _)          => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SKP(_)                => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SKNP(_)               => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::LDDT2reg(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDKey(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDreg2DT(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDreg2ST(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADDI(_)               => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDFont(_)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDBCB(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::PUSHI(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::POPI(_)               => Chip8Instruction::several_next_addresses(current_address, 1),
        }
    }

    // for control flow graph construction, we need to explore all pathes for each call stack
    pub fn cfg_next(&self, current_address: MemoryAddress, stack: &mut Vec<MemoryAddress>) -> Result<HashSet<MemoryAddress>, Error> {
        match self {
            Chip8Instruction::CLS                   => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::RET                   => {
                let add = stack.pop().ok_or(format_err!("Empty stack @ {} (RET)", current_address))?;
                Chip8Instruction::this_address(add)
            },
            Chip8Instruction::JP(add)               => Chip8Instruction::this_address(*add),
            Chip8Instruction::CALL(add)             => {
                let mut next = current_address;
                next.next_inst()?;
                stack.push(next);
                Chip8Instruction::this_address(*add)
            },
            Chip8Instruction::SEi(_, _)             => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SE(_, _)              => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SNEi(_, _)            => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SNE(_ , _)            => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::LDi(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADDi(_, _)            => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::MOV(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::OR(_, _)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::AND(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::XOR(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADD(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SUB(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SUBN(_, _)            => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SHR(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SHL(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDI(_)                => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::JPV0Off(add)          => Chip8Instruction::jpv0off_next_addresses(*add),
            Chip8Instruction::RND(_, _)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::DRW(_, _, _)          => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::SKP(_)                => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::SKNP(_)               => Chip8Instruction::several_next_addresses(current_address, 2),
            Chip8Instruction::LDDT2reg(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDKey(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDreg2DT(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDreg2ST(_)           => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::ADDI(_)               => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDFont(_)             => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::LDBCB(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::PUSHI(_)              => Chip8Instruction::several_next_addresses(current_address, 1),
            Chip8Instruction::POPI(_)               => Chip8Instruction::several_next_addresses(current_address, 1),
        }
    }
}

impl fmt::Display for Chip8Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Chip8Instruction::CLS                 => write!(f, "CLS"),
            Chip8Instruction::RET                 => write!(f, "RET"),
            Chip8Instruction::JP(add)             => write!(f, "JP {}", add),
            Chip8Instruction::CALL(add)           => write!(f, "CALL {}", add),
            Chip8Instruction::SEi(vx, imm)        => write!(f, "SEi {}, {}", vx, imm),
            Chip8Instruction::SE(vx, vy)          => write!(f, "SE {}, {}", vx, vy),
            Chip8Instruction::SNEi(vx, imm)       => write!(f, "SNEi {}, {}", vx, imm),
            Chip8Instruction::SNE(vx, vy)         => write!(f, "SNE {}, {}", vx, vy),
            Chip8Instruction::LDi(vx, imm)        => write!(f, "LDi {}, {}", vx, imm),
            Chip8Instruction::ADDi(vx, imm)       => write!(f, "ADDi {}, {}", vx, imm),
            Chip8Instruction::MOV(vx, vy)         => write!(f, "MOV {}, {}", vx, vy),
            Chip8Instruction::OR(vx, vy)          => write!(f, "OR {}, {}", vx, vy),
            Chip8Instruction::AND(vx, vy)         => write!(f, "AND {}, {}", vx, vy),
            Chip8Instruction::XOR(vx, vy)         => write!(f, "XOR {}, {}", vx, vy),
            Chip8Instruction::ADD(vx, vy)         => write!(f, "ADD {}, {}", vx, vy),
            Chip8Instruction::SUB(vx, vy)         => write!(f, "SUB {}, {}", vx, vy),
            Chip8Instruction::SUBN(vx, vy)        => write!(f, "SUBN {}, {}", vx, vy),
            Chip8Instruction::SHR(vx, vy)         => write!(f, "SHR {}, {}", vx, vy),
            Chip8Instruction::SHL(vx, vy)         => write!(f, "SHL {}, {}", vx, vy),
            Chip8Instruction::LDI(add)            => write!(f, "LDI {}", add),
            Chip8Instruction::JPV0Off(add)        => write!(f, "JPV0Off {}", add),
            Chip8Instruction::RND(vx, imm)        => write!(f, "RND {}, {}", vx, imm),
            Chip8Instruction::DRW(vx, vy, nibble) => write!(f, "DRW {}, {}, {}", vx, vy, nibble),
            Chip8Instruction::SKP(vx)             => write!(f, "SKP {}", vx),
            Chip8Instruction::SKNP(vx)            => write!(f, "SKNP {}", vx),
            Chip8Instruction::LDDT2reg(vx)        => write!(f, "LDDT2reg {}", vx),
            Chip8Instruction::LDKey(vx)           => write!(f, "LDKey {}", vx),
            Chip8Instruction::LDreg2DT(vx)        => write!(f, "LDreg2DT {}", vx),
            Chip8Instruction::LDreg2ST(vx)        => write!(f, "LDreg2ST {}", vx),
            Chip8Instruction::ADDI(vx)            => write!(f, "ADDI {}", vx),
            Chip8Instruction::LDFont(vx)          => write!(f, "LDFont {}", vx),
            Chip8Instruction::LDBCB(vx)           => write!(f, "LDBCB {}", vx),
            Chip8Instruction::PUSHI(vx)           => write!(f, "PUSHI {}", vx),
            Chip8Instruction::POPI(vx)            => write!(f, "POPI {}", vx),

            // _ => write!(f, "UNKNOWN")
        }
    }
}

#[test]
fn test_0x0xxx() {
    let clear = 0x00E0;
    assert_eq!(Chip8Instruction::parse_instruction(clear).unwrap(), Chip8Instruction::CLS);

    let ret = 0x00EE;
    assert_eq!(Chip8Instruction::parse_instruction(ret).unwrap(), Chip8Instruction::RET);

    let err = 0x0000;
    assert!(Chip8Instruction::parse_instruction(err).is_err());
}

#[test]
fn test_0x1xxx() {
    let jp = 0x1234;
    assert_eq!(Chip8Instruction::parse_instruction(jp).unwrap(), Chip8Instruction::JP(MemoryAddress::from_val(0x234).unwrap()));
}

#[test]
fn test_0x2xxx() {
    let call = 0x2234;
    assert_eq!(Chip8Instruction::parse_instruction(call).unwrap(), Chip8Instruction::CALL(MemoryAddress::from_val(0x234).unwrap()));
}

#[test]
fn test_0x3xxx() {
    let sei = 0x3456;
    assert_eq!(
        Chip8Instruction::parse_instruction(sei).unwrap(), 
        Chip8Instruction::SEi(
            RegisterIndex::from_name("V4").unwrap(),
            0x56)
        );
}

#[test]
fn test_0x4xxx() {
    let sei = 0x4456;
    assert_eq!(
        Chip8Instruction::parse_instruction(sei).unwrap(), 
        Chip8Instruction::SNEi(
            RegisterIndex::from_name("V4").unwrap(),
            0x56)
        );
}

#[test]
fn test_0x5xxx() {
    let se = 0x5230;
    assert_eq!(
        Chip8Instruction::parse_instruction(se).unwrap(), 
        Chip8Instruction::SE(
            RegisterIndex::from_name("V2").unwrap(),
            RegisterIndex::from_name("V3").unwrap())
        );
}

#[test]
fn test_0x6xxx() {
    let ld = 0x6345;
    assert_eq!(
        Chip8Instruction::parse_instruction(ld).unwrap(), 
        Chip8Instruction::LDi(
            RegisterIndex::from_name("V3").unwrap(),
            0x45)
        );
}


#[test]
fn test_0x7xxx() {
    let inst = 0x7456;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::ADDi(
            RegisterIndex::from_name("V4").unwrap(),
            0x56)
        );
}

#[test]
fn test_0x8xxx() {
    let inst = 0x8673;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::XOR(
            RegisterIndex::from_name("V6").unwrap(),
            RegisterIndex::from_name("V7").unwrap())
        );
}

#[test]
fn test_0x9xxx() {
    let se = 0x9230;
    assert_eq!(
        Chip8Instruction::parse_instruction(se).unwrap(), 
        Chip8Instruction::SNE(
            RegisterIndex::from_name("V2").unwrap(),
            RegisterIndex::from_name("V3").unwrap())
        );
}

#[test]
fn test_0xaxxx() {
    let inst = 0xA123;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::LDI(
            MemoryAddress::from_val(0x123).unwrap())
        );
}

#[test]
fn test_0xbxxx() {
    let inst = 0xB123;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::JPV0Off(
            MemoryAddress::from_val(0x123).unwrap())
        );
}

#[test]
fn test_0xcxxx() {
    let inst = 0xC123;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::RND(
            RegisterIndex::from_name("V1").unwrap(),
            0x23)
        );
}

#[test]
fn test_0xdxxx() {
    let inst = 0xD123;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::DRW(
            RegisterIndex::from_name("V1").unwrap(),
            RegisterIndex::from_name("V2").unwrap(),
            0x3)
        );
}

#[test]
fn test_0xexxx() {
    let inst = 0xE39E;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::SKP(
            RegisterIndex::from_name("V3").unwrap())
        );
}

#[test]
fn test_0xfxxx() {
    let inst = 0xF31E;
    assert_eq!(
        Chip8Instruction::parse_instruction(inst).unwrap(), 
        Chip8Instruction::ADDI(
            RegisterIndex::from_name("V3").unwrap())
        );
}