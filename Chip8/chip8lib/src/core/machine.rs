/*
 * File: machine.rs
 * Project: src
 * Created Date: Friday June 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 25th July 2018 12:16:42 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;
use io::*;

use std::path::Path;

pub trait Machine {
    fn reset(&mut self);

    fn cycle(&mut self) -> Result<(), Error>;

    /// Inject a random fault at next opcode
    fn inject_fault(&mut self, opcode: u16);

    fn run_n_cycles(&mut self, n: usize) -> Result<(), Error> {
        for _ in 0..n {
            self.cycle()?;
        }
        Ok(())
    }

    fn display(&mut self) -> &mut Display;

    fn keypad(&mut self) -> &mut Keypad;

    fn buzzer(&mut self) -> &mut Buzzer;

    fn load_program_file(&mut self, path: &Path) -> Result<(usize, usize), Error>;//return size of rom
}