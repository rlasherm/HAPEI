/*
 * File: mod.rs
 * Project: cfg
 * Created Date: Monday July 2nd 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 2nd July 2018 5:11:30 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

pub mod cfg;

pub use self::cfg::{CFG, GraphStack};