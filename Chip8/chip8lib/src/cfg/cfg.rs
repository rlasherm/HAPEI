/*
 * File: cfi.rs
 * Project: src
 * Created Date: Friday June 29th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 5:18:49 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use std::collections::{BTreeMap, HashSet};
use std::collections::hash_map::DefaultHasher;
use std::fs::File;
use std::io::Write;
use std::fmt;
use std::path::Path;
use std::hash::{Hash, Hasher};

use core::*;
use failure::Error;

#[derive(Debug,Clone,Hash,PartialEq,Eq,PartialOrd,Ord)]
pub struct GraphStack(Vec<MemoryAddress>);

impl GraphStack {
    pub fn new() -> GraphStack {
        GraphStack(Vec::new())
    }

    // pub fn push(&mut self, add: MemoryAddress) {
    //     self.0.push(add);
    // }

    // pub fn pop(&mut self) -> Option<MemoryAddress> {
    //     self.0.pop()
    // }

    pub fn compute_hash(&self) -> u64 {
        let mut s = DefaultHasher::new();
        // self.hash(&mut s);

        if self.0.len() > 0 {
            self.0[0].hash(&mut s);
        }

        for i in 1..self.0.len() {
            if self.0[i] == self.0[i-1] { //this special condition allows to deal with recursive calls: if two successive calls, then hash is identical and CFG exploration stopg
                // println!("Recursive call detected !");
                break;
            }
            else {
                self.0[i].hash(&mut s);
            }
        }
        s.finish()
    }
}

impl fmt::Display for GraphStack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Stack({})", self.0.iter().map(|a|a.to_string()).collect::<Vec<String>>().join(", "))
    }
}

/// Control Flow Graph
pub struct CFG {
    pub instructions: BTreeMap<MemoryAddress, Chip8Instruction>,
    pub next_addresses: BTreeMap<MemoryAddress, HashSet<MemoryAddress>>,
    pub previous_stack_state: BTreeMap<MemoryAddress, HashSet<u64>>//the whole stack is hash into a value that represent its whole state -> if current stack already explored, hash must be present here

}

impl CFG {

    pub fn new() -> CFG {
        CFG { instructions: BTreeMap::new(), next_addresses: BTreeMap::new(), previous_stack_state: BTreeMap::new() }
    }

    pub fn instructions_count(&self) -> usize {
        self.instructions.len()
    }

    pub fn writecfg2file<T: AsRef<Path>>(&self, filepath: T) -> Result<(), Error> {
        let mut file = File::create(filepath)?;

        for (add, inst) in self.instructions.iter() {
            let next_adds = if let Some(next_set) = self.next_addresses.get(add) {
                next_set.iter().map(|a| a.to_string()).collect::<Vec<String>>().join(", ")
            }
            else {
                String::new()
            };

            write!(&mut file, "{} {}        |-> {}\n", add, inst, next_adds)?;
        }

        Ok(())
    }

    pub fn get_predecessors(&self, start_address: MemoryAddress) -> BTreeMap<MemoryAddress, HashSet<MemoryAddress>> {
        let mut res = BTreeMap::new();

        for (add, nexts) in self.next_addresses.iter() {
            for n in nexts.iter() {
                let mut predecessors = res.remove(n).unwrap_or(HashSet::new());
                predecessors.insert(*add);
                res.insert(*n, predecessors);
            }
        }

        //add entry to predecessor
        let mut predecessors = res.remove(&start_address).unwrap_or(HashSet::new());
        predecessors.insert(MemoryAddress::entry());
        res.insert(start_address, predecessors);

        res
    }

    pub fn writedis2file<T: AsRef<Path>>(disassemble: BTreeMap<MemoryAddress, Chip8Instruction>, filepath: T) -> Result<(), Error> {
        let mut file = File::create(filepath)?;

        for (add, inst) in disassemble.iter() {
            write!(&mut file, "{} {}\n", add, inst)?;
        }

        Ok(())
    }

    pub fn disassemble(memory: &[u8], start_add: MemoryAddress) -> Result<BTreeMap<MemoryAddress, Chip8Instruction>, Error> {
        let mut instructions: BTreeMap<MemoryAddress, Chip8Instruction> = BTreeMap::new();

        CFG::recursive_disassemble(memory, &mut instructions, start_add)?;

        Ok(instructions)
    }

    fn recursive_disassemble(memory: &[u8], instructions: &mut BTreeMap<MemoryAddress, Chip8Instruction>, inst_add: MemoryAddress) -> Result<(), Error> {
        if instructions.get(&inst_add).is_some() {
            return Ok(());
        }
        else {
            let opcode = CFG::next_opcode(memory, inst_add);
            let inst = Chip8Instruction::parse_instruction(opcode)?;

            instructions.insert(inst_add, inst);
            if let Ok(next_addresses) = inst.dis_next(inst_add) {
                 //recursive descent
                for next_add in next_addresses.iter() {
                    CFG::recursive_disassemble(memory, instructions, *next_add)?;
                }
            }

            Ok(())
        }
    }

    pub fn extract(memory: &[u8], start_add: MemoryAddress) -> Result<CFG, Error> {
        

        let stack = GraphStack::new();
        let mut cfg = CFG::new();
        cfg.recursive_explore(stack, memory, start_add)?;
        // CFG::recursive_add_inst(memory, &mut stack, &mut instructions, start_add)?;

        Ok(cfg)
    }

    fn recursive_explore(&mut self, mut stack: GraphStack, memory: &[u8], inst_add: MemoryAddress) -> Result<(), Error> {
        //check if early return
        let current_hash = stack.compute_hash();

        let mut previous_hashes = self.previous_stack_state.remove(&inst_add).unwrap_or({
            HashSet::new()
        });

        // if stack state already explored -> early return
        let early_return = previous_hashes.contains(&current_hash);
        if early_return {
            //put data back in cfg
            self.previous_stack_state.insert(inst_add, previous_hashes);
            return Ok(());
        }

        //this is a new state
        previous_hashes.insert(current_hash);

        // parse current opcode (if new or not)
        let inst = self.instructions.remove(&inst_add).unwrap_or({
            let opcode = CFG::next_opcode(memory, inst_add);
            let inst = Chip8Instruction::parse_instruction(opcode)?;
            inst
        });

        let previous_next = self.next_addresses.remove(&inst_add).unwrap_or({
            HashSet::new()
        });

        let next_addresses = inst.cfg_next(inst_add, &mut stack.0)?;
        let next_union: HashSet<MemoryAddress> = previous_next.union(&next_addresses).map(|a|*a).collect();

        // //debug start
        // let debug_next = next_addresses.iter().map(|a|a.to_string()).collect::<Vec<String>>().join(", ");
        // let debug_prev = previous_next.iter().map(|a|a.to_string()).collect::<Vec<String>>().join(", ");
        // let debug_next = next_union.iter().map(|a|a.to_string()).collect::<Vec<String>>().join(", ");
        // let debug_hashes = previous_hashes.iter().map(|a|a.to_string()).collect::<Vec<String>>().join(", ");

        // match inst {
        //     Chip8Instruction::RET => {
        //         println!("***** {} RET -> {}  | {}", inst_add, debug_next, stack);
        //     },
        //     Chip8Instruction::CALL(add) => {
        //         println!("***** {} CALL -> {}  | {}", inst_add, add, stack);
        //     }
        //     _ => {
        //         println!("{} {}           -> {}  | {}", inst_add, inst, debug_next, stack);
        //     }
        // }
        
        // //debug end

        //fill cfg
        self.instructions.insert(inst_add, inst);
        self.next_addresses.insert(inst_add, next_union.clone());
        self.previous_stack_state.insert(inst_add, previous_hashes);

        //recursive descent
        for next_add in next_addresses.iter() {
            self.recursive_explore(stack.clone(), memory, *next_add)?;
        }
        
        Ok(())
    }

    fn next_opcode(memory: &[u8], inst_add: MemoryAddress) -> u16 {
        (memory[inst_add.index()] as u16) << 8 | (memory[inst_add.index() + 1] as u16)
    }
}