/*
 * File: main.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 11:52:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[macro_use] extern crate log;
#[macro_use] extern crate failure;
extern crate rand;
extern crate chip8lib;

pub mod cpu;
