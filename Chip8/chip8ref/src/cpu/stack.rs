/*
 * File: stack.rs
 * Project: cpu
 * Created Date: Friday June 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 8th June 2018 9:40:16 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

const STACK_DEPTH: usize = 16;

use chip8lib::core::*;

#[derive(Debug)]
pub struct Stack {
    stack: [MemoryAddress; STACK_DEPTH],
    stack_pointer: usize
}

impl Stack {
    pub fn new() -> Stack {
        Stack { stack: [MemoryAddress(0); STACK_DEPTH], stack_pointer: 0 }
    }

    pub fn reset(&mut self) {
        self.stack = [MemoryAddress(0); STACK_DEPTH];
        self.stack_pointer = 0;
    }

    pub fn push(&mut self, add: MemoryAddress) {
        self.stack[self.stack_pointer] = add;
        self.stack_pointer += 1;
    }

    pub fn pop(&mut self) -> MemoryAddress {
        self.stack_pointer -= 1;
        self.stack[self.stack_pointer]
    }
}