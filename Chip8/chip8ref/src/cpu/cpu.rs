/*
 * File: machine.rs
 * Project: src
 * Created Date: Tuesday June 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 26th July 2018 11:52:56 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::Error;
use rand;

use std::fs::File;
use std::io::Read;
use std::fmt;
use std::path::Path;

use chip8lib::core::*;
use chip8lib::io::*;
use chip8lib::helpers::*;
use chip8lib::cfg::CFG;

use cpu::*;

const START_ADDRESS: MemoryAddress = MemoryAddress(0x200);
const FONT_ADDRESS: MemoryAddress = MemoryAddress(0x0);

const FONTSET: [u8; 80] = [0xF0, 0x90, 0x90, 0x90, 0xF0, 0x20, 0x60, 0x20, 0x20, 0x70,
0xF0, 0x10, 0xF0, 0x80, 0xF0, 0xF0, 0x10, 0xF0, 0x10, 0xF0,
0x90, 0x90, 0xF0, 0x10, 0x10, 0xF0, 0x80, 0xF0, 0x10, 0xF0,
0xF0, 0x80, 0xF0, 0x90, 0xF0, 0xF0, 0x10, 0x20, 0x40, 0x40,
0xF0, 0x90, 0xF0, 0x90, 0xF0, 0xF0, 0x90, 0xF0, 0x10, 0xF0,
0xF0, 0x90, 0xF0, 0x90, 0x90, 0xE0, 0x90, 0xE0, 0x90, 0xE0,
0xF0, 0x80, 0x80, 0x80, 0xF0, 0xE0, 0x90, 0x90, 0x90, 0xE0,
0xF0, 0x80, 0xF0, 0x80, 0xF0, 0xF0, 0x80, 0xF0, 0x80, 0x80];

/// CHIP-8 virtual machine
/// Following http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/ and mikezaby/chip-8
pub struct Cpu {
    /// The main working memory
    ram: [u8; MEMORY_SIZE as usize],

    /// The 16 "V" registers
    v: RegisterBank,

    /// The index register
    i: MemoryAddress,

    /// The Program Counter
    pc: MemoryAddress,

    /// The stack, as an external memory (owns stack pointer)
    stack: Stack,

    /// Delay timer
    dt: TimedRegister8,

    /// Sound timer
    st: TimedRegister8,
    
    /// Keypad
    keypad: Keypad,
    
    /// Display
    display: Display,

    /// Buzzer
    buzzer: Buzzer,

    // For timing related things
    stopwatch: Stopwatch
}

impl Machine for Cpu {
    fn reset(&mut self) {
        self.i = START_ADDRESS;
        self.pc = START_ADDRESS;
        self.stack.reset();
        self.dt.reset();
        self.st.reset();
        self.v.reset();

        self.keypad.reset();
        self.display.clear();
        self.buzzer.reset();
        self.stopwatch.reset();

        for i in 0..80 {
            self.ram[FONT_ADDRESS.index() + i] = FONTSET[i];
        }
    }

    fn cycle(&mut self) -> Result<(), Error> {
        let opcode = self.next_opcode();
        let inst = Chip8Instruction::parse_instruction(opcode)?;

        trace!("@{} -> [0x{:04x}] {}", self.pc, opcode, inst);
        // println!("@{} [0x{:04x}] {} -> ", self.pc, opcode, inst);
        self.execute_instruction(inst)?;

        // println!("");

        // duration from previous tick
        let elapsed = self.stopwatch.tick();
        self.dt.tick(elapsed);
        self.st.tick(elapsed);

        Ok(())
    }

    fn inject_fault(&mut self, opcode: u16) {
        let pc = self.pc;
        self.write_opcode_at(opcode, pc);
    }


    fn display(&mut self) -> &mut Display {
        &mut self.display
    }

    fn keypad(&mut self) -> &mut Keypad {
        &mut self.keypad
    }

    fn buzzer(&mut self) -> &mut Buzzer {
        &mut self.buzzer
    }

    fn load_program_file(&mut self, path: &Path) -> Result<(usize, usize), Error> {
        let mut file = File::open(path).map_err(|e|format_err!("{}", e))?;
        let mut buffer = Vec::new();
        file.read_to_end(&mut buffer).map_err(|e|format_err!("Cannot read file {}: {}", path.display(), e))?;
        let len = buffer.len();

        let icount = self.load_to_memory(&buffer)?;
        Ok((len, icount))
    }
}

impl Cpu {
    pub fn new() -> Cpu {
        let mut machine = Cpu {
            ram: [0; MEMORY_SIZE as usize],
            v: RegisterBank::new(),
            i: START_ADDRESS,
            pc: START_ADDRESS,
            stack: Stack::new(),
            dt: TimedRegister8::new(),
            st: TimedRegister8::new(),
            keypad: Keypad::new(),
            display: Display::new(),
            buzzer: Buzzer::new(),
            stopwatch: Stopwatch::start()
        };

        machine.reset();
        machine
    }

    fn write_opcode_at(&mut self, new_opcode: u16, add: MemoryAddress) {
        self.ram[add.index()] = ((new_opcode >> 8) & 0xFF) as u8;
        self.ram[add.index() + 1] = (new_opcode & 0xFF) as u8;
    }

    fn load_to_memory(&mut self, to_load: &[u8]) -> Result<usize, Error> {
        for b in to_load.iter() {
            self.ram[self.pc.index()] = *b;
            self.pc.offset(1)?;
        }

        let cfg = CFG::extract(&self.ram, START_ADDRESS)?;

        Ok(cfg.instructions_count())
    }

    /// Read memory and decode instructon
    fn next_opcode(&self) -> u16 {
        (self.ram[self.pc.index()] as u16) << 8 | (self.ram[self.pc.index() + 1] as u16)
    }

    /// In this function, the virtual machine is actually implemented...
    fn execute_instruction(&mut self, inst: Chip8Instruction) -> Result<(), Error> {
        match inst {
            Chip8Instruction::CLS => {
                self.display.clear();
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::RET => {
                // set pc to previous address in stack
                self.pc = self.stack.pop();
                Ok(())
            },

            Chip8Instruction::JP(add) => {
                self.pc = add;
                Ok(())
            },

            Chip8Instruction::CALL(add) => {
                // save pc in stack, then jump
                self.pc.next_inst()?;
                self.stack.push(self.pc);
                self.pc = add;
                Ok(())
            },

            Chip8Instruction::SEi(vx, imm) => {
                if self.v.get(vx) == imm {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::SNEi(vx, imm) => {
                if self.v.get(vx) != imm {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::SE(vx, vy) => {
                if self.v.get(vx) == self.v.get(vy) {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::SNE(vx, vy) => {
                if self.v.get(vx) != self.v.get(vy) {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::LDi(vx, imm) => {
                self.v.set(vx, imm);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::ADDi(vx, imm) => {
                let new_val = self.v.get(vx) + imm;
                self.v.set(vx, new_val);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::MOV(vx, vy) => {
                let new_val = self.v.get(vy);
                self.v.set(vx, new_val);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::OR(vx, vy) => {
                let new_val = self.v.get(vx) | self.v.get(vy);
                self.v.set(vx, new_val);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::AND(vx, vy) => {
                let new_val = self.v.get(vx) & self.v.get(vy);
                self.v.set(vx, new_val);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::XOR(vx, vy) => {
                let new_val = self.v.get(vx) ^ self.v.get(vy);
                self.v.set(vx, new_val);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::ADD(vx, vy) => {
                let new_val = self.v.get(vx) + self.v.get(vy);
                self.v.set(vx, new_val);
                let flag = if self.v.get(vx) < self.v.get(vy) { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::SUB(vx, vy) => {
                let new_val = self.v.get(vx) - self.v.get(vy);
                self.v.set(vx, new_val);
                let flag = if self.v.get(vx) > self.v.get(vy) { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::SHR(vx, _) => {
                let new_val = self.v.get(vx) >> 1;
                self.v.set(vx, new_val);
                let flag = if (self.v.get(vx) & 0x1) != 0 { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::SUBN(vx, vy) => {
                let new_val = self.v.get(vy) - self.v.get(vx);
                self.v.set(vx, new_val);
                let flag = if self.v.get(vy) > self.v.get(vx) { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::SHL(vx, _) => {
                let new_val = self.v.get(vx) << 1;
                self.v.set(vx, new_val);
                let flag = if (self.v.get(vx) & 0x80) != 0 { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::LDI(add) => {
                self.i = add;
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::JPV0Off(mut add) => {
                add.offset(self.v.get(V0) as u16)?;
                self.pc = add;
                Ok(())
            },

            Chip8Instruction::RND(vx, imm) => {
                self.v.set(vx, rand::random::<u8>() & imm);//TODO allow replay
                self.pc.next_inst()?;
                Ok(())
            },
            
            Chip8Instruction::DRW(vx,vy,nibble) => {
                let from = self.i.index();
                let to = from + nibble as usize;
                let sprite = &self.ram[from..to];

                let loc = DisplayLocation::xy(self.v.get(vx) as usize, self.v.get(vy) as usize);

                let collision = self.display.draw_sprite(loc, sprite);
                let flag = if collision { 1 } else { 0 };
                self.v.set(VF, flag);
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::SKP(vx) => {
                let key = Key::from_val(self.v.get(vx))?;
                if self.keypad.pressed(key) == true {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::SKNP(vx) => {
                let key = Key::from_val(self.v.get(vx))?;
                if self.keypad.pressed(key) == false {
                    self.pc.next_multiple_inst(2)?;
                }
                else {
                    self.pc.next_inst()?;
                }
                Ok(())
            },

            Chip8Instruction::LDDT2reg(vx) => {
                self.v.set(vx, self.dt.get());
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::LDKey(vx) => {
                if let Some(key) = self.keypad.first_pressed() {
                    self.v.set(vx, key.0);
                    self.pc.next_inst()?;
                }
                // else we do not update pc -> this instruction will be called again until a key is pressed
                // still allowing us to update events betwee "execute_instruction" calls
                Ok(())
            },

            Chip8Instruction::LDreg2DT(vx) => {
                self.dt.set(self.v.get(vx));
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::LDreg2ST(vx) => {
                self.st.set(self.v.get(vx));
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::ADDI(vx) => {
                self.i.offset(self.v.get(vx) as u16)?;
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::LDFont(vx) => {
                let digit = self.v.get(vx) as u16;
                let mut digit_font_add = FONT_ADDRESS;
                digit_font_add.offset(digit * 5)?;
                self.i = digit_font_add;
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::LDBCB(vx) => {
                //stolen from mikezaby/chip-8 (github)
                self.ram[self.i.index()] = self.v.get(vx) / 100;
                self.ram[self.i.index() + 1] = (self.v.get(vx) / 10) % 10;
                self.ram[self.i.index() + 2] = (self.v.get(vx) % 100) % 10;
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::PUSHI(vx) => {
                let max_v = vx.index();
                for reg in 0..=max_v {
                    self.ram[self.i.index() + reg] = self.v.get(RegisterIndex(reg as u8));
                }
                // self.i.offset(max_v as u16 + 1)?;
                self.pc.next_inst()?;
                Ok(())
            },

            Chip8Instruction::POPI(vx) => {
                let max_v = vx.index();
                for reg in 0..=max_v {
                    self.v.set(RegisterIndex(reg as u8), self.ram[self.i.index() + reg]);
                }
                // self.i.offset(max_v as u16 + 1)?;
                self.pc.next_inst()?;
                Ok(())
            },

            // _ => Err(format_err!("Unimplemented instruction {:?}@{:?}", inst, self.pc))
        }
    }
}

impl fmt::Display for Cpu {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "R{:?}, PC={}, I={}", self.v, self.pc, self.i)
    }
}