# Hardware-Assisted Program Execution Integrity: HAPEI (Companion repository)

In this repository, you can find:
- the source for the hardened and reference CHIP-8 virtual machine,
- the NordSec 2018 paper.