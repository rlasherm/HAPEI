\documentclass[11pt]{beamer}
\usetheme{CambridgeUS}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{tikz}
\usepackage{array}
\author[R. Lashermes]{\textbf{Ronan Lashermes}, Hélène Le Bouder and Gaël Thomas}
\title[HAPEI]{Hardware-Assisted Program Execution Integrity: HAPEI}
\date{June 22nd, 2018}
\graphicspath{{figures/}}
\beamertemplatenavigationsymbolsempty
%\setbeamercovered{transparent} 
% \setbeamertemplate{navigation symbols}{} 
%\logo{} 

\date{ November 29th, 2018}
\institute[]{INRIA, IMT-Atlantique, DGA}

%\subject{} 

\AtBeginSection{\frame{\sectionpage}}

\begin{document}

\begin{frame}
\includegraphics[height=1cm,width=2cm]{inria-logo.png}
\titlepage

\begin{center}
NordSec 2018, Oslo
\end{center}


\end{frame}

\begin{frame}{Content}
    \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Instructions}
    \center{\includegraphics[width=0.75\textwidth]{instructions.png}}
\end{frame}

\begin{frame}
    \begin{huge}
        \center{The software abstraction is a lie}
    \end{huge}
    ~\\~\\
    Ok, it works for simple architectures

    \center{\includegraphics[width=0.2\textwidth]{cake.jpg}}
\end{frame}

\begin{frame}{What is hardware ?}
    \center{\includegraphics[width=0.75\textwidth]{rocket_core.png}}\\
    Rocket core (RISC-V)
\end{frame}

\begin{frame}{Fallacies about hardware}
    \begin{enumerate}
        \item Memory access is $O(1)$ in time $\Rightarrow$ Cache timing attacks.
        \item Instructions are executed in order $\Rightarrow$ Spectre/Meltdown/...
        \item Program integrity is guaranteed
    \end{enumerate}
\end{frame}
    
\begin{frame}
    \only<1>{
        \begin{block}{Hardware fault attacks}
            \center{\includegraphics[height=0.6\textheight]{faustine.jpg}}\\
            \tiny{\textcopyright Inria / Photo C. Morel} 
        \end{block}
    }

    \only<2> {
        \center{\includegraphics[height=0.8\textheight]{faustine_zoom.jpg}}\\
        \tiny{\textcopyright Inria / Photo C. Morel}
    }

\end{frame}

\begin{frame}[fragile]{Fault activated backdoor}
    \begin{tiny}
    \begin{block}{Source code}
        \begin{lstlisting}[language=c]
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
        \end{lstlisting}
    \end{block}

    \begin{block}{Assembly}
        \begin{lstlisting}
08000598 <blink_wait>:
push    {r7, lr}
sub    sp, #8
add    r7, sp, #0
ldr    r3, [pc, #44]    ; (80005cc <blink_wait+0x34>)
...
adds    r7, #8
mov    sp, r7
pop    {r7, pc}
.word    0xe00be00c ; @80005cc, 0xe00be00c = 3758874636
        \end{lstlisting}
    \end{block}
\end{tiny}
\end{frame}

\begin{frame}[fragile]{Fault activated backdoor}
    \begin{tiny}
    \begin{block}{Source code}
        \begin{lstlisting}[language=c]
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
        \end{lstlisting}
    \end{block}

    \begin{block}{Assembly}
        \begin{lstlisting}
08000598 <blink_wait>:
push    {r7, lr}
sub    sp, #8
add    r7, sp, #0
ldr    r3, [pc, #44]    ; (80005cc <blink_wait+0x34>)
...
adds    r7, #8
mov    sp, r7
nop
b backdoor
        \end{lstlisting}
    \end{block}
\end{tiny}
\end{frame}

\begin{frame}
    \center{The previous application could have been proven correct.}
\end{frame}

\section{Ensuring program execution integrity}

\begin{frame}
    \begin{block}{Several integrities}
        \begin{itemize}
            \item \textbf{Instructions Integrity (II)}: executed instructions belong to the program.
            \item \textbf{Control Flow Integrity (CFI)}: only authorized control flow (jumps, branches, ...).
            \item \textbf{Data Integrity (DI)}: program data cannot be tampered with.
            \item Program Integrity = II + CFI
        \end{itemize}
    \end{block}

    \begin{block}{Several attack models}
        \begin{itemize}
            \item \textbf{Code Injection Attacks (CIA)}: an attacker tries to divert the control flow to execute its own malicious payload.
            \item \textbf{Code-Reuse Attacks (CRA)}: an attacker tries to execute a malicious payload composed by a sequence of legitimate pieces of programs (often called widgets).
            \item \textbf{Hardware Fault Injection (HFI)}: the attacker can edit the program, at runtime, by modifiyng data or instruction values.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{SOFIA: Instruction Set Randomization\footnote{"SOFIA: Software and control flow integrity architecture", de Clercq et al., 2016}}
    \begin{block}{Control Flow Integrity}
        Encrypt instructions, with program state encoding:
        $$i' = E_k(PC_{prev} || PC) \oplus i$$
    \end{block}
    \begin{block}{Example}
        \begin{lstlisting}
            1: i1'
            2: i2'
            3: i3'
        \end{lstlisting}
        To decrypt $i_3$: $i_3=E_k(2 || 3) \oplus i'_3$
    \end{block}
    Nice if all instructions have only 1 predecessor... If not we have a special case to deal with.

    
\end{frame}

\begin{frame}{SOFIA: Instruction Set Randomization\footnote{"SOFIA: Software and control flow integrity architecture", de Clercq et al., 2016}}
    \begin{block}{System Integrity}
        Compute a MAC for all 6-instructions blocks, placed at the beginning of the block.        
    \end{block}

    \begin{columns}[T] % align columns
        \begin{column}{.3\textwidth}
            \center{\includegraphics[height=0.45\textheight]{sofia2p.png}}
        \end{column}%
        \hfill%
        \begin{column}{.6\textwidth}
            \begin{block}{2-predecessors}
                Compute the two corresponding MACs and adapt the control flow to the entry point.
            \end{block}
        \end{column}%
    \end{columns}
\end{frame}

\begin{frame}{HAPEI: Hardware-Assisted Program Execution Integrity}
    \vspace{-0.3cm}
    \begin{block}{Goal}
        Another solution to the CFI\&II problem, yet inefficient (for now). Instruction Set Randomization technique.
    \end{block}

    \begin{block}{1) PC is not the program state}
        $$acc_n = HMAC_k(acc_{n-1}||i_{n-1}).$$
        Initial state can be $acc_0 = HMAC_k(IV)$.
        $k$ device specific secret key.
    \end{block}

    \begin{block}{2) Encrypt with program state (as in SOFIA)}
        Encrypt: $$i'_n = C(acc_n) \oplus i_n.$$
        $C$ is a compression fonction: the size of $acc_n$ must be decided according to security requirements.
    \end{block}
    
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{3) 2-predecessors}
        The state of the program before a 2-predecessors instruction must be an invariant depending on the 2 possible states. Program state values $\in \mathbb{F}_{2^b}$.
        
        Encrypt: $$\{ \Sigma=a_1 \oplus a_2, i'_n = C(a_1 \cdot a_2) \oplus i_n \}.$$
        Decrypt: $$i_n = C\left(acc_n \cdot (acc_n \oplus \Sigma )\right) \oplus i'_n.$$

        $\Sigma$ gives no information away on $a_1$ or $a_2$ if both stay secret.
    \end{block}

    \only<2->{
    \begin{tikzpicture}[remember picture,overlay]
        \node at (current page.center) {\includegraphics[width=5cm]{figures/warn.png}};
    \end{tikzpicture}}
\end{frame}



\begin{frame}{HAPEI}
    \vspace{-0.3cm}
    \begin{block}{4) n-predecessors (cycles allowed in CFG)}
        The state of the program before a n-predecessors instruction must be a random invariant (rebase). We must be able to project all legitimate program states to this rebased value, and reject illegitimate values.
        ~\\~\\
        \onslide<2->{
        Solution: use projection into subgroups of $\mathbb{F}_{2^b}$. Subgroup of size $r$ exists $\forall r | 2^b-1$.
        
        Example: $5 | 2^{16}-1$, so there is a cyclic subgroup $\{\mu, \mu^2, \mu^3, \mu^4, \mu^5=1 \}$ for some $\mu \in \mathbb{F}_{2^b}$ with $\mu^r=1$.
        ~\\~\\
        }
        \onslide<3->{
        Encrypt (5-predecessors): $a_1, a_2, \ldots, a_5$. Choose random $c \in \mathbb{F}_{2^b}$. Compute polynomial $P$ of degree $4$ such that: $$P(a_i) = c\cdot \mu^i.$$
        Store $\{ P, i'_n = C(c^r) \oplus i_n \}.$
        }
    \end{block}
    
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{Decrypt}
        $$i_n = C\left( P(acc_n)^r \right) \oplus i'_n.$$
        
        Works because $\forall i$, $$P(a_i)^r = \left( c \cdot \mu^i \right)^r = c^r \cdot \left( \mu^r \right)^i = c^r.$$
        Exponentiation by $r$ required to keep degree of $P$ minimal (but not equal to constant).
    \end{block}
\end{frame}
    
\begin{frame}{HAPEI}
    \begin{block}{Why the exponentiation? Memory efficiency and security}
    	It is possible to devise a polynomial to map directly from all $a_i$ to $c$ (and $1$ to $1$).
    	But then, the polynomial gives information on its roots. $\forall a, deg(gcd(P[x]-a,x^{2^b} -x)) \leq 5$, but $deg(gcd(P[x]-c,x^{2^b} -x))=5$ for the correct roots.
    \end{block}
    
    \pause
    \begin{block}{Polynomial Exponentiation Trick}
    	$deg(gcd(P[x]-c\cdot \mu^i,x^{2^b} -x)) \leq 4$ but no information leaks as easily.
    	
    	\textbf{But it means that illegitimate program states can be accepted !} 
    	It supposes that the attacker cannot control the program state value.
    \end{block}
    
\end{frame}

\begin{frame}{CHIP-8 implementation}
    \begin{block}{CHIP-8}
        It is an Instruction Set Architecture (ISA) for a video games 8-bit virtual machine (from the 1970s).
        Extremely simple ISA ($\approx$ 30 instructions).
        ROMs (video games binaries) are freely available on internet.
    \end{block}

    \begin{block}{Our implementation}
        Two implementations: the reference and the hardened one.
        A special key press modifies the next opcode with a random valid one.
    \end{block}
\end{frame}

\begin{frame}
	\begin{center}
		\begin{Large}
			Demo time !
		\end{Large}
	\end{center}
\end{frame}

\begin{frame}{Hardening memory usage for a set of CHIP-8 roms.}
    \begin{table}
        \vspace{-0.5cm}
        \begin{scriptsize}
        \begin{tabular}{ | >{\centering\arraybackslash}p{2cm} | >{\centering\arraybackslash}p{1.4cm} | >{\centering\arraybackslash}p{1.6cm} | >{\centering\arraybackslash}p{1.6cm} | >{\centering\arraybackslash}p{1.4cm} | >{\centering\arraybackslash}p{1.7cm} | }
            \hline
            ROM name & ROM byte size & Instructions count & Polynomials count & Field elements & Polynomials byte size \\
            \hline
            INVADERS & 1283 & 202 & 28 & 99 & 1584 \\
            GUESS & 148 & 49 & 8 & 25 & 400 \\
            KALEID & 120 & 59 & 10 & 32 & 512 \\
            CONNECT4 & 194 & 67 & 5 & 19 & 304 \\
            WIPEOFF & 206 & 101 & 15 & 47 & 752 \\
            PONG2 & 264 & 126 & 19 & 60 & 960 \\
            15PUZZLE & 384 & 116 & 17 & 54 & 864 \\
            TETRIS & 494 & 189 & 32 & 106 & 1696 \\
            BLINKY & 2356 & 856 & 84 & 310 & 4960 \\
            VBRIX & 507 & 218 & 27 & 93 & 1488 \\
            SYZYGY & 946 & 414 & 44 & 149 & 2384 \\
            BRIX & 280 & 134 & 17 & 57 & 912 \\
            TICTAC & 486 & 194 & 23 & 89 & 1424 \\
            MAZE & 34 & 13 & 3 & 10 & 160 \\
            PUZZLE & 184 & 87 & 10 & 34 & 544 \\
            BLITZ & 391 & 121 & 15 & 47 & 752 \\
            VERS & 230 & 103 & 24 & 73 & 1168 \\
            PONG & 246 & 117 & 18 & 57 & 912 \\
            UFO & 224 & 106 & 15 & 48 & 768 \\
            TANK & 560 & 236 & 42 & 139 & 2224 \\
%            MISSILE & 180 & 75 & 12 & 37 & 592 \\
%            HIDDEN & 850 & 258 & 24 & 81 & 1296 \\
%            MERLIN & 345 & 124 & 13 & 45 & 720 \\
            \hline
        \end{tabular}
    \end{scriptsize}
    \end{table}
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{Limitations}
        \begin{itemize}
            \item Indirect branches: \textbf{`ADD PC, PC, R1'},
            \item dynamic libraries,
            \item system calls,
            \item load time/run time relocation\ldots
        \end{itemize}
    \end{block}

    \begin{block}{}
        The whole architecture (hardware, software, \ldots) has to be designed with security in mind.
    \end{block}
\end{frame}

\section{Conclusion}

\begin{frame}{Where the difficulty lies}
    To prove a solution secure in an abstract model is not enough.
    ~\\~\\
    It must be implemented without flaws at all abstractions levels.
\end{frame}

\begin{frame}{This is not a hardware problem...}

    ... it is a cross abstraction problem.

    \begin{itemize}
        \item Information leaks over networks: you can perform cache timing attacks on a distant server.
        \item RowHammer faults on a co-hosted virtual machine.
        \item Protocol prover : ``cryptographic primitives are supposed unbreakable''.
    \end{itemize}

    Technologies like \textit{Trustzone} or \textit{SGX} do not help in that regard.
    
\end{frame}

\begin{frame}{Conclusion}
    Cross abstraction vulnerabilities reflect our human inability to grasp complexity.
    
    This abstraction layers representation is an illustration of the sociological dimension of systems design.

    We need new tools to ensure security across layers.
\end{frame}

\begin{frame}{Thank you!}
    \center{Any questions?}
    \center{\includegraphics[height=0.6\textheight]{faustine_zoom.jpg}}\\
    \tiny{© Inria / Photo C. Morel}
\end{frame}

\end{document}
