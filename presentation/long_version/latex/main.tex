\documentclass[11pt]{beamer}
\usetheme{CambridgeUS}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{tikz}
\author[R. Lashermes]{\textbf{Ronan Lashermes}, Hélène Le Bouder and Gaël Thomas}
\title[Program Execution Integrity]{The program execution integrity problem}
\date{June 22nd, 2018}
\graphicspath{{figures/}}
\beamertemplatenavigationsymbolsempty
%\setbeamercovered{transparent} 
% \setbeamertemplate{navigation symbols}{} 
%\logo{} 
%\institute{} 
%\date{} 
%\subject{} 

\AtBeginSection{\frame{\sectionpage}}

\begin{document}

\begin{frame}
\includegraphics[height=1cm,width=2cm]{inria-logo.png}
\titlepage

from the LHS/SED @ INRIA-RBA
\end{frame}

%\begin{frame}
%\tableofcontents
%\end{frame}

\section*{Introduction}

\begin{frame}{``Find the vulnerabilities'' game}
\center{
    \includegraphics{verify-pin.png}
}
\end{frame}

\begin{frame}
    \begin{huge}
        \center{Is this software vulnerable only because of the progammer's mistakes ?}
    \end{huge}
    ~\\~\\
    \visible<2->{Or, given a specification, is a perfect software possible with respect to vulnerabilities ?}
\end{frame}

\begin{frame}{Content}
    \tableofcontents
\end{frame}

\section{Software}

\begin{frame}{What is software ?}
    \center{\includegraphics{source_code.png}}
\end{frame}

\begin{frame}{Instructions}
    \center{\includegraphics[width=0.75\textwidth]{instructions.png}}
\end{frame}

\begin{frame}{The hardware/software interface}
    \center{\includegraphics[width=0.9\textwidth]{hsi.png}}
    ~\\
    \begin{block}{Conway's law}
        Organizations which design systems are constrained to produce designs which are copies of the communication structures of these organizations.
    \end{block}

    \begin{block}{}
        In an alternate universe, we have FPGA IPs per application.
    \end{block}
\end{frame}

\begin{frame}{Instruction Set Architecture}
    \center{\includegraphics[width=0.7\textwidth]{armv7m.png}}
\end{frame}

\begin{frame}
    \begin{huge}
        \center{Is there a perfect program given the specifications ?}
    \end{huge}
    ~\\~\\
    $\rightarrow$ Formal methods
\end{frame}

\begin{frame}
    \begin{huge}
        \center{The software abstraction is a lie}
    \end{huge}
    ~\\~\\
    Ok, it works for simple architectures

    \center{\includegraphics[width=0.2\textwidth]{cake.jpg}}
\end{frame}

\section{Hardware}

\begin{frame}{What is hardware ?}
    \center{\includegraphics[width=0.75\textwidth]{rocket_core.png}}\\
    Rocket core (RISC-V)
\end{frame}

% \begin{frame}{The hardware/software interface}
%     \center{\includegraphics[width=0.8\textwidth]{hsi.png}}
%     ~\\
%     \begin{block}{Conway's law}
%         Organizations which design systems are constrained to produce designs which are copies of the communication structures of these organizations.
%     \end{block}

%     \begin{block}{}
%         In an alternate universe, we have FPGA IPs per application.
%     \end{block}
% \end{frame}

\begin{frame}{Fallacy 1: Memory access is $O(1)$ in time}
    \center{\includegraphics[width=0.5\textwidth]{mem_time.png}}\\
    \begin{tiny}
        From \url{www.ilikebigbits.com/blog/2014/4/21/the-myth-of-ram-part-i}
    \end{tiny}

    \begin{block}{Theory}
        The Bekenstein bound says that $N \propto r \cdot m$.
        Maximum mass (black hole) proportional to radius so $N \propto r^2$ (in accordance with holographic principle).

        Access time is therefore $\propto r$: $O(\sqrt{N})$.
    \end{block}
    
\end{frame}

\begin{frame}{Fallacy 2: Instructions are executed in order}
    \begin{tiny}
        \textit{Still holds for simplest architectures.}
    \end{tiny}

    \only<1> {
        \begin{block}{1) Out-of-order execution}
            \center{\includegraphics[width=0.75\textwidth]{outoforder.png}}
        \end{block}
    }

    \only<2> {
        \begin{block}{2) Speculative execution}
            \center{\includegraphics[height=0.6\textheight]{speculative.png}}
        \end{block}
    }
    
\end{frame}

\begin{frame}{Fallacy 3: Program execution integrity is guaranteed}
    \center{\includegraphics[width=0.75\textwidth]{iss.jpg}}\\
    \tiny{© NASA} 
\end{frame}

\section{Abusing hardware}

\begin{frame}{Fallacy 1: Memory access is $O(1)$ in time}
    \begin{block}{Cache timing attacks (FLUSH+RELOAD)}
        \begin{itemize}
            \item Empty the cache.
            \item Let the victim execute.
            \item Try to access a memory value.
            \item Mesure access timing $\rightarrow$ whether the victim has accessed this address.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Fallacy 2: Instructions are executed in order}
        \begin{block}{1) Out-of-order execution: Meltdown attack\footnote{``Meltdown", Lipp et al., 2018}}
            \begin{lstlisting}[language=c]
raise_exception();
// the line below is never reached
access(probe_array[data*4096]);
        	\end{lstlisting}
        	
        	\center{\includegraphics[width=0.6\textwidth]{meltdown_access.png}}
        \end{block}
\end{frame}

\begin{frame}[fragile]{Fallacy 2: Instructions are executed in order}


        \begin{block}{2) Speculative execution: Spectre attack\footnote{``Spectre Attacks: Exploiting Speculative Execution", Kocher et al., 2018}}
            \begin{lstlisting}[language=c]
if (x < array1_size)
    y = array2[array1[x] * 256];
            \end{lstlisting}

            Then train the system to predict the vulnerable branch.
        \end{block}
    
    
\end{frame}

\begin{frame}{Fallacy 3: program integrity is guaranteed}

    \only<1>{
        \begin{block}{Hardware fault attacks}
            \center{\includegraphics[height=0.6\textheight]{faustine.jpg}}\\
            \tiny{© Inria / Photo C. Morel} 
        \end{block}
    }

    \only<2> {
        \center{\includegraphics[height=0.8\textheight]{faustine_zoom.jpg}}\\
        \tiny{© Inria / Photo C. Morel}
    }
    
\end{frame}

\begin{frame}[fragile]{Control flow highjacking}

    \begin{block}{Source code}
        \begin{lstlisting}[language=c]
            if(correct == 1) {
                status = 0xFFFFFFFF; }
            else {
                status = 0x55555555; }
            \end{lstlisting}
    \end{block}

    \begin{block}{Assembly}
        \begin{lstlisting}
        cmp r3, #1         ; r3 contains [correct]
        ite eq                      ; if then else
        moveq.w r4, #4294967295     ; 0xffffffff
        movne.w r4, #1431655765     ; 0x55555555
        \end{lstlisting}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Fault activated backdoor}
    \begin{tiny}
    \begin{block}{Source code}
        \begin{lstlisting}[language=c]
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
        \end{lstlisting}
    \end{block}

    \begin{block}{Assembly}
        \begin{lstlisting}
08000598 <blink_wait>:
push    {r7, lr}
sub    sp, #8
add    r7, sp, #0
ldr    r3, [pc, #44]    ; (80005cc <blink_wait+0x34>)
...
adds    r7, #8
mov    sp, r7
pop    {r7, pc}
.word    0xe00be00c ; @80005cc, 0xe00be00c = 3758874636
        \end{lstlisting}
    \end{block}
\end{tiny}
\end{frame}

\begin{frame}[fragile]{Fault activated backdoor}
    \begin{tiny}
    \begin{block}{Source code}
        \begin{lstlisting}[language=c]
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
        \end{lstlisting}
    \end{block}

    \begin{block}{Assembly}
        \begin{lstlisting}
08000598 <blink_wait>:
push    {r7, lr}
sub    sp, #8
add    r7, sp, #0
ldr    r3, [pc, #44]    ; (80005cc <blink_wait+0x34>)
...
adds    r7, #8
mov    sp, r7
nop
b backdoor
        \end{lstlisting}
    \end{block}
\end{tiny}
\end{frame}

\begin{frame}
    \center{The previous application could have been proven correct.}
    \only<2-> {
    \center{We need better abstractions to develop programs.}
    }
\end{frame}

\section{Ensuring program execution integrity}

\begin{frame}
    \begin{block}{Several integrities}
        \begin{itemize}
            \item \textbf{Instructions Integrity (II)}: executed instructions belong to the program.
            \item \textbf{Control Flow Integrity (CFI)}: only authorized control flow (jumps, branches, ...).
            \item \textbf{Data Integrity (DI)}: program data cannot be tampered with.
            \item \ldots
        \end{itemize}
    \end{block}

    \begin{block}{Several attack models}
        \begin{itemize}
            \item \textbf{Code Injection Attacks (CIA)}: an attacker tries to divert the control flow to execute its own malicious payload.
            \item \textbf{Code-Reuse Attacks (CRA)}: an attacker tries to execute a malicious payload composed by a sequence of legitimate pieces of programs (often called widgets).
            \item \textbf{Hardware Fault Injection (HFI)}: the attacker can edit the program, at runtime, by modifiyng data or instruction values.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]{SOFIA: Instruction Set Randomization\footnote{"SOFIA: Software and control flow integrity architecture", de Clercq et al., 2016}}
    \begin{block}{Control Flow Integrity}
        Encrypt instructions, with program state encoding:
        $$i' = E_k(PC_{prev} || PC) \oplus i$$
    \end{block}
    \begin{block}{Example}
        \begin{lstlisting}
            1: i1'
            2: i2'
            3: i3'
        \end{lstlisting}
        To decrypt $i_3$: $i_3=E_k(2 || 3) \oplus i'_3$
    \end{block}
    Nice if all instructions have only 1 predecessor... If not we have a special case to deal with.

    
\end{frame}

\begin{frame}{SOFIA: Instruction Set Randomization\footnote{"SOFIA: Software and control flow integrity architecture", de Clercq et al., 2016}}
    \begin{block}{System Integrity}
        Compute a MAC for all 6-instructions blocks, placed at the beginning of the block.        
    \end{block}

    \begin{columns}[T] % align columns
        \begin{column}{.3\textwidth}
            \center{\includegraphics[height=0.45\textheight]{sofia2p.png}}
        \end{column}%
        \hfill%
        \begin{column}{.6\textwidth}
            \begin{block}{2-predecessors}
                Compute the two corresponding MACs and adapt the control flow to the entry point.
            \end{block}
        \end{column}%
    \end{columns}
\end{frame}

\begin{frame}{HAPEI: Hardware-Assisted Program Execution Integrity}
    \vspace{-0.3cm}
    \begin{block}{Goal}
        Elegant solution to the CFI\&II problem (DI in the future?), yet inefficient (for now). Instruction Set Randomization technique.
    \end{block}

    \begin{block}{1) PC is not the program state}
        $$acc_n = HMAC_k(acc_{n-1}||i_{n-1}).$$
        Initial state can be $acc_0 = HMAC_k(IV)$.
        $k$ device specific secret key.
    \end{block}

    \begin{block}{2) Encrypt with program state (as in SOFIA)}
        Encrypt: $$i'_n = C(acc_n) \oplus i_n.$$
        $C$ is a compression fonction: the size of $acc_n$ must be decided according to security requirements.
    \end{block}
    
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{3) 2-predecessors}
        The state of the program before a 2-predecessors instruction must be an invariant depending on the 2 possible states. Program state values $\in \mathbb{F}_{2^b}$.
        
        Encrypt: $$\{ \Sigma=a_1 \oplus a_2, i'_n = C(a_1 \cdot a_2) \oplus i_n \}.$$
        Decrypt: $$i_n = C\left(acc_n \cdot (acc_n \oplus \Sigma )\right) \oplus i'_n.$$

        $\Sigma$ gives no information away on $a_1$ or $a_2$ if both stay secret.
    \end{block}

    \only<2->{
    \begin{tikzpicture}[remember picture,overlay]
        \node at (current page.center) {\includegraphics[width=5cm]{figures/warn.png}};
    \end{tikzpicture}}
\end{frame}



\begin{frame}{HAPEI}
    \vspace{-0.3cm}
    \begin{block}{4) n-predecessors (cycles allowed in CFG)}
        The state of the program before a n-predecessors instruction must be a random invariant (rebase). We must be able to project all legitimate program state to this rebased value, and reject illegitimate values.
        ~\\~\\
        \only<2->{
        Solution: use projection into subgroups of $\mathbb{F}_{2^b}$. Subgroup of size $r$ exists $\forall r | 2^b-1$.
        
        Example: $5 | 2^{16}-1$, so there is a cyclic subgroup $\{\mu, \mu^2, \mu^3, \mu^4, \mu^5=1 \}$ for some $\mu \in \mathbb{F}_{2^b}$.
        ~\\~\\
        }
        \only<3->{
        Encrypt (4-predecessors): $a_1, a_2, a_3, a_4$. Choose random $c \in \mathbb{F}_{2^b}$. Compute polynomial $P$ of degree $3$ such that: $$P(a_i) = c\cdot \mu^i.$$
        Store $\{ P, i'_n = C(c^r) \oplus i_n \}.$
        }
    \end{block}
    
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{Decrypt}
        $$i_n = C\left( P(acc_n)^r \right) \oplus i'_n.$$
        
        Works because $\forall i$, $$P(a_i)^r = \left( c \cdot \mu^i \right)^r = c^r \cdot \left( \mu^r \right)^i = c^r.$$
        Exponentiation by $r$ required to keep degree of $P$ minimal (but not equal to constant).
    \end{block}
\end{frame}
    
\begin{frame}{HAPEI}
    \begin{block}{Why the exponentiation? Memory efficiency and security}
    	It is possible to devise a polynomial to map directly from all $a_i$ to $c$ (and $1$ to $1$).
    	But then, the polynomial gives information on its roots. $\forall a, deg(gcd(P[x]-a,x^{2^b} -x)) \leq 4$, but $deg(gcd(P[x]-c,x^{2^b} -x))=4$ for the correct roots.
    \end{block}
    
    \pause
    \begin{block}{Polynomial Exponentiation Trick}
    	$deg(gcd(P[x]-c\cdot \mu^i,x^{2^b} -x)) \leq 3$ but no information leaks as easily.
    	
    	\textbf{But it means that illegitimate program states can be accepted !} 
    	It supposes that the attacker cannot control the program state value.
    \end{block}
    
    \begin{alertblock}{Work in progress}
        The security of this scheme has yet to be proven.
        Implementation incoming... (CHIP-8 virtual machine).
    \end{alertblock}
\end{frame}

\begin{frame}{HAPEI}
    \begin{block}{Limitations}
        \begin{itemize}
            \item Indirect branches: \textbf{ADD PC, PC, R1},
            \item dynamic libraries,
            \item system calls,
            \item load time/run time relocation\ldots
        \end{itemize}
    \end{block}

    \begin{block}{}
        The whole architecture (hardware, software, \ldots) has to be designed with security in mind.
    \end{block}
\end{frame}

\section{Conclusion}

\begin{frame}{Where the difficulty lies}
    To prove the proposed solution secure in an abstract model is not enough.
    ~\\~\\
    It must be implemented without flaws at all abstractions levels.
\end{frame}

\begin{frame}{This is not a hardware problem...}

    ... it is a cross abstraction problem.

    \begin{itemize}
        \item Information leaks over networks: you can perform cache timing attacks on a distant server.
        \item RowHammer faults on a co-hosted virtual machine.
        \item Protocol prover : ``cryptographic primitives are supposed unbreakable''.
    \end{itemize}

    Technologies like \textit{Trustzone} or \textit{SGX} do not help in that regard.
    
\end{frame}

\begin{frame}{Conclusion}
    Cross abstraction vulnerabilities reflect our human inability to grasp complexity.
    This abstraction layers representation is an illustration of the sociological dimension of systems design.

    We need new tools to ensure security across layers.
\end{frame}

\begin{frame}{Thank you!}
    \center{Any questions?}
    \center{\includegraphics[width=0.4\textwidth]{iss.jpg}}\\
    \tiny{© NASA} 
\end{frame}

\end{document}