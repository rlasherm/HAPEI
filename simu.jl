using Nemo

# m = 16
m=62
pt_count = 2
F, x = FiniteField(2, m, "x")
a = map(x-> rand(F), 1:pt_count)
# push!(a, F(1))

d5=Int64((2^m-1)/5)
u5 = rand(F)^d5
r=rand(F)

b=map(x-> r*u5^x, 1:pt_count)
# b = map(x->r, 1:4)
# push!(b, F(1))

P, p = PolynomialRing(F, "p")

function Lagrange(x, y)
    deg = length(x) 
    sum = P(0)

    for i = 1:deg
        sum = sum + y[i]*Lbasis(i, x, y)
    end
    return sum
end

function Lbasis(j, x, y)
    deg = length(x)
    acc = P(1)

    for m = 1:deg
        if m != j
            acc = acc * (p-x[m]) * (x[j] - x[m])^-1
        end
    end
    return acc
end

function randSolve(Poly, y, maxTry=1000000)
    tryCounter = 0

    g = rand(F)
    current = g

    res = []

    while tryCounter < maxTry
        
        if Poly(current) == y 
            if !(current in res)
                push!(res, current)
            end
        end

        tryCounter += 1
        current *= g

        if tryCounter % 1000 == 0
            print(".")
            g = rand(F)

            if length(res) > 1000
                return res
            end
        end
    end
    return res
end

function el_order(r, maxTry=1000000)
    tryCounter = 1
    current = r

    while tryCounter < maxTry
        current *= r
        tryCounter += 1

        if current == r
            return tryCounter
        end
    end
    -1
end

L1 = Lagrange(a, b)