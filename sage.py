'''
File: sage.py
Project: Paper
Created Date: Tuesday June 19th 2018
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 6th July 2018 2:48:42 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2018 INRIA
'''
m = 128
d = 5

predecessors = 3

assert( (2^m-1) % d == 0)
F = GF(2^m, 'x')
P = PolynomialRing(F, 'p')

a = map(lambda i: F.random_element(), range(0,predecessors))
a.append(F(1))
big_subgroup_exp =int((2^m-1)/d)
ud = F.random_element()^big_subgroup_exp
rd = F.random_element()

# b = map(lambda i: rd*ud^i, range(1,predecessors+1))
b = map(lambda i: rd, range(0,predecessors))
b.append(F(1))

L1 = P.lagrange_polynomial(map(lambda x,y: (x,y), a, b))

r1s=(L1-rd).roots()


def rootsdist(d):
    av = 500
    count = {}

    for i in range(0,d+1):
        count[i] = 0

    for i in range(0,av):
        p = P.random_element(d)
        r = len(p.roots())
        count[r] += 1

    for k in count.keys():
        count[k] = float(count[k]/av)

    return count

def rootsperdegree(d):
    av = 500
    count = 0
    for i in range(0,av):
        p = P.random_element(d)
        count = count + len(p.roots())

    return float(count/av)